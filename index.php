<?php header("Content-Type: text/html; charset=utf-8"); ?>
<script type="text/javascript" src="js/jquery/jquery-2.0.2.js"></script>
<script type="text/javascript" src="js/jquery/jqueryrotate.2.1.js"></script>
<script type="text/javascript" src="js/comet/CometServerApi.js"></script>

<script type="text/javascript" src="js/inheritance.js"></script>
<script type="text/javascript" src="js/steps.js"></script>

<script type="text/javascript" src="js/commandsPlay.js"></script>
<script type="text/javascript" src="js/environs.js"></script>
<script type="text/javascript" src="js/universalFunction.js"></script>
<script type="text/javascript" src="js/theMap.js"></script>
<script type="text/javascript" src="js/ObjectOfEnvirons.js"></script>
<script type="text/javascript" src="js/independentInanimateObjects.js"></script>
<script type="text/javascript" src="js/independentLivingObjects.js"></script>
<script type="text/javascript" src="js/Wight.js"></script>

<script type="text/javascript" src="js/events.js"></script>
<script type="text/javascript" src="js/remoteUnit.js"></script>
<script type="text/javascript" src="js/spots.js"></script>
<script type="text/javascript" src="js/shooting.js"></script>
<script type="text/javascript" src="js/globalAI.js"></script>
<script type="text/javascript" src="js/AI.js"></script>

<script type="text/javascript" src="js/Bonus.js"></script>
<script type="text/javascript" src="js/SlowBullet.js"></script>
<script type="text/javascript" src="js/BotOne.js"></script>

<script type="text/javascript" src="js/unitApi.js"></script>
<html>
    <head>
        <script type="text/javascript">
               
        

        //--------- Функция движения элемента elem -------------------------------------------------------------------------------------------//

        /**
         * Функция движения объекта stepObject(elem)
         * @param {object} elem - движимый объект
         */
        function stepObject(elem){
            // Если нажата одна клавиша движения
            if(keyActiveArray.length === 1){
                // Выбор команды
                commandsMoveKey[keyActiveArray[0]](elem);
            }
            else if(keyActiveArray.length === 2){// Если нажаты две клавиши движения
                // Поиск комманд
                if(typeof commandsMoveKey[keyActiveArray[0]+""+keyActiveArray[1]] !== 'undefined'){commandsMoveKey[keyActiveArray[0]+""+keyActiveArray[1]](elem);}
                if(typeof commandsMoveKey[keyActiveArray[1]+""+keyActiveArray[0]] !== 'undefined'){commandsMoveKey[keyActiveArray[1]+""+keyActiveArray[0]](elem);}
            }
            else
            {
                commandsMoveKey['_STOP'](elem);
            }

        }

        CometServer().start({dev_id:2 /*, user_id:2, user_key:"hdgaciipt35vc3ksda5b5e91q2"*/ })

        //---------- Описание единого пространства -----------------------------------------------------------------------------------------------------------//


            $(document).ready(function(){

                //extend(independentInanimateObjects, ObjectOfEnvirons);  // Наследование independentInanimateObjects от ObjectOfEnvirons
                //extend(Wight, ObjectOfEnvirons);                        // Наследование Wight от ObjectOfEnvirons
                //extend(Spots, ObjectOfEnvirons);                        // Наследование Spots от ObjectOfEnvirons
                //extend(AI, Wight);                                      // Наследование AI от Wight

                gameMap = new theMap({W: 800, H: 800}, {W: 800, H: 800}); // Единая карта пространства
                gameMap.CreateSectors(3);


                $("#environs").css({'height' : gameMap.getSizeWindow().H, 'width' : gameMap.getSizeWindow().W});    // Размеры окна
                $("#main").css({'height' : gameMap.getSizeEnvirons().H, 'width' : gameMap.getSizeWindow().W});      // Размеры пространства

                //gameMap.remoteUnitApi = new unitApi();
                gameMap.StartMainLoop();

                // Создание интелектуальных агентов
                globaInteligens = {};
                
                globaInteligens.evil = new globalAI("evil", "evil"); // Агент зла
                globaInteligens.good = new globalAI("good", "good"); // Агент добра
                
                
                
                
                // Объект "Стрелок"
                striker = new Wight("striker", "striker", "striker");
                striker.addCoordinates(50, 250);
                striker.addSize(32, 32);
                striker.createElement("main");
                striker.addStyle();
                striker.addStyle("backgroundImage", "url(img/glaz.png)");
                striker.addStyle("border", "0");
                striker.health = 1000;
//
//                rtest1 = new remoteUnit("rtest1", "rtest1", "striker");
//                rtest1.createElement("main");
//                rtest1.addCoordinates(767, 34);
//                rtest1.addSize(32, 32);
//                rtest1.addStyle();
//                rtest1.addStyle("backgroundImage", "url(img/glaz.png)");
//                rtest1.addStyle("border", "0");
//                gameMap.remoteUnitApi.bind("rtest1", striker)


                var BotOneArr = []
                for(var i = 0; i< 3; i++)
                {
                    BotOneArr[i]= new BotOne("BotOne_"+i, "BotOne_"+i, "BotOne");
                    BotOneArr[i].createElement("main");
                    BotOneArr[i].addCoordinates(430 + i*60, 50);
                    BotOneArr[i].addSize(14, 14);
                    BotOneArr[i].addStyle();
                    BotOneArr[i].addStyle("backgroundImage", "url(img/BotOne.gif)");
                    BotOneArr[i].addStyle("border", "2px solid");
                    BotOneArr[i].addStyle("border-radius", "0px");
                    BotOneArr[i].setEnemies(["BotTwo", "commandaA", "commandaB", "striker"])
                    BotOneArr[i].addMathCord()
                }
                
                var BotTwoArr = []
                for(var i = 0; i< 3; i++)
                {
                    BotTwoArr[i]= new BotOne("BotTwo_"+i, "BotTwo_"+i, "BotTwo");
                    BotTwoArr[i].createElement("main");
                    BotTwoArr[i].addCoordinates(350 + i*60, 380);
                    BotTwoArr[i].addSize(14, 14);
                    BotTwoArr[i].addStyle();
                    BotTwoArr[i].addStyle("backgroundImage", "url(img/BotTwo.gif)");
                    BotTwoArr[i].addStyle("border", "2px solid");
                    BotTwoArr[i].addStyle("border-radius", "0px");
                    BotTwoArr[i].setEnemies(["BotOne", "commandaA", "commandaB", "striker"])
                    BotTwoArr[i].addMathCord()
                }
                
//                BonusArr = []
//                        
//                for(var i = 0; i< 1; i++)
//                {
//                    BonusArr[i]= new Bonus("Bonus_"+i, "Bonus_"+i, "striker");
//                    BonusArr[i].createElement("main");
//                    BonusArr[i].addCoordinates(370 + i*60, 50);
//                    BonusArr[i].addSize(14, 14);
//                    BonusArr[i].addStyle();
//                    BonusArr[i].addStyle("backgroundImage", "url(img/HealthBonus2x14.png)");
//                    BonusArr[i].addStyle("border", "1px solid #ccc");
//                    BonusArr[i].addStyle("border-radius", "60px");
//                }




                gameMap.mainEnvirons.element = document.getElementById("main"); // Записываем DIV main как пространство

//                var top = new ObjectOfEnvirons("top", "top", "element");
//                top.addCoordinates(0, 20);
//                top.addSize(900, 10);
//                top.createElement("main");
//                top.addStyle();
//                top.visibleCoord();
//                top.addClass("StaticElement")
//                top.addStyle("overflow", " hidden");
//                top.addMathCord()
//
//                var bottom = new ObjectOfEnvirons("bottom", "bottom", "element");
//                bottom.addCoordinates(0, 770);
//                bottom.addSize(900, 10);
//                bottom.createElement("main");
//                bottom.addStyle();
//                bottom.visibleCoord();
//                bottom.addClass("StaticElement")
//                bottom.addStyle("overflow", " hidden");
//                bottom.addMathCord()
//
//                var left = new ObjectOfEnvirons("left", "left", "element");
//                left.addCoordinates(20, 0);
//                left.addSize(10, 900);
//                left.createElement("main");
//                left.addStyle();
//                left.visibleCoord();
//                left.addClass("StaticElement")
//                left.addStyle("overflow", " hidden");
//                left.addMathCord()
//                
//                var right = new ObjectOfEnvirons("right", "right", "element");
//                right.addCoordinates(770, 0);
//                right.addSize(10, 900);
//                right.createElement("main");
//                right.addStyle();
//                right.visibleCoord();
//                right.addClass("StaticElement")
//                right.addStyle("overflow", " hidden");
//                right.addMathCord()
                
                //--------- Создание множества объектов на пространства --------------------------------------------------------------------------------------//
                one = new ObjectOfEnvirons("one", "one", "element");
                one.addCoordinates(0, 100);
                one.addSize(200, 100);
                one.createElement("main");
                one.addStyle();
                one.visibleCoord();
                one.addClass("StaticElement")
                one.addMathCord();

                two = new independentInanimateObjects("two", "two", "element");
                two.addCoordinates(175, 125);
                two.addSize(100, 200);
                two.createElement("main");
                two.addStyle();
                two.visibleCoord();
                two.addClass("StaticElement")
                two.addMathCord();

                three = new independentInanimateObjects("three", "three", "element");
                three.addCoordinates(350, 100);
                three.addSize(200, 100);
                three.createElement("main");
                three.addStyle();
                three.visibleCoord();
                three.addClass("StaticElement")
                three.addMathCord();

                four = new independentInanimateObjects("four", "four", "element");
                four.addCoordinates(100, 270);
                four.addSize(200, 100);
                four.createElement("main");
                four.addStyle();
                four.visibleCoord();
                four.addClass("StaticElement")
                four.addMathCord();

                five = new independentInanimateObjects("five", "five", "element");
                five.addCoordinates(0, 450);
                five.addSize(400, 100);
                five.createElement("main");
                five.addStyle();
                five.visibleCoord();
                five.addClass("StaticElement")
                five.addMathCord();

                six = new independentInanimateObjects("six", "six", "element");
                six.addCoordinates(400, 200);
                six.addSize(100, 150);
                six.createElement("main");
                six.addStyle();
                six.visibleCoord();
                six.addClass("StaticElement")
                six.addMathCord();
                
                seven = new independentInanimateObjects("seven", "seven", "element");
                seven.addCoordinates(600, 100);
                seven.addSize(100, 300);
                seven.createElement("main");
                seven.addStyle();
                seven.visibleCoord();
                seven.addClass("StaticElement")
                seven.addMathCord();
                


//                seven = new independentInanimateObjects("seven", "seven", "element");
//                seven.addCoordinates(800, 100);
//                seven.addSize(10, 10);
//                seven.createElement("main");
//                seven.addStyle();
//                seven.visibleCoord();
//                seven.addStyle("backgroundColor", "cornflowerblue");
                //--------------------------------------------------------------------------------------------------------------------------------------------//
 
                
                // Объект "AI"
                Genius = new AI("Genius", "Genius", "commandaA", "evil");
                Genius.addCoordinates(50, 50);
                Genius.addSize(32, 32);
                Genius.createElement("main");
                Genius.addStyle();
                Genius.addStyle("backgroundImage", "url(img/glaz.png)");
                Genius.addStyle("border-radius", "40px");
                Genius.addStyle("border", "4px solid #f77");
                Genius.pointStartAI = {X:Genius.center.X, Y:Genius.center.Y};
                Genius.Armor = Armor["пистолет"];
//                Genius.mainPorpose = {X: 730, Y: 400};
                Genius.findWay();  
                //Genius.findShortWay({X: 730, Y: 400})
                
                // Объект "AI"
                Genius2 = new AI("Genius2", "Genius2", "commandaA", "evil");
                Genius2.addCoordinates(700, 50);
                Genius2.addSize(32, 32);
                Genius2.createElement("main");
                Genius2.addStyle();
                Genius2.addStyle("backgroundImage", "url(img/glaz.png)");
                Genius2.addStyle("border-radius", "40px");
                Genius2.addStyle("border", "4px solid #f77");
                Genius2.pointStartAI = {X:Genius2.center.X, Y:Genius2.center.Y};
                Genius2.Armor = Armor["пистолет"];
                Genius2.findWay(); 
                
                
                // Объект "AI"
                Genius3 = new AI("Genius3", "Genius3", "commandaA", "evil");
                Genius3.addCoordinates(350, 50);
                Genius3.addSize(32, 32);
                Genius3.createElement("main");
                Genius3.addStyle();
                Genius3.addStyle("backgroundImage", "url(img/glaz.png)");
                Genius3.addStyle("border-radius", "40px");
                Genius3.addStyle("border", "4px solid #f77");
                Genius3.pointStartAI = {X:Genius3.center.X, Y:Genius3.center.Y};
                Genius3.Armor = Armor["дробовик"];
                Genius3.findWay();  
//                
//                
//                // Объект "AI"
                Genius4 = new AI("Genius4", "Genius4", "commandaB", "good");
                Genius4.addCoordinates(600, 600);
                Genius4.addSize(32, 32);
                Genius4.createElement("main");
                Genius4.addStyle();
                Genius4.addStyle("backgroundImage", "url(img/glaz.png)");
                Genius4.addStyle("border-radius", "40px");
                Genius4.addStyle("border", "4px solid #7f7");
                Genius4.pointStartAI = {X:Genius4.coordinates.X-10, Y:Genius4.coordinates.Y-10};
                Genius4.Armor = Armor["пистолет"];
                Genius4.findWay(); 
//                
//                // Объект "AI"
                Genius5 = new AI("Genius5", "Genius5", "commandaB", "good");
                Genius5.addCoordinates(350, 600);
                Genius5.addSize(32, 32);
                Genius5.createElement("main");
                Genius5.addStyle();
                Genius5.addStyle("backgroundImage", "url(img/glaz.png)");
                Genius5.addStyle("border-radius", "40px");
                Genius5.addStyle("border", "4px solid #7f7");
                Genius5.pointStartAI = {X:Genius5.center.X, Y:Genius5.center.Y};
                Genius5.Armor = Armor["пистолет"];
                Genius5.findWay();  
//                
//                // Объект "AI"
                Genius6 = new AI("Genius6", "Genius6", "commandaB", "good");
                Genius6.addCoordinates(50, 700);
                Genius6.addSize(32, 32);
                Genius6.createElement("main");
                Genius6.addStyle();
                Genius6.addStyle("backgroundImage", "url(img/glaz.png)");
                Genius6.addStyle("border-radius", "40px");
                Genius6.addStyle("border", "4px solid #7f7");
                Genius6.pointStartAI = {X:Genius6.center.X, Y:Genius6.center.Y};
                Genius6.Armor = Armor["дробовик"]
                Genius6.findWay();  
                // Объект "AI"
//                superGenius = new AI("superGenius", "superGenius", "commandaB");
//                superGenius.addCoordinates(600, 500);
//                superGenius.addSize(32, 32);
//                superGenius.createElement("main");
//                superGenius.addStyle();
//                superGenius.addStyle("backgroundImage", "url(img/glaz.png)");
//                superGenius.addStyle("border", "0");
//                superGenius.pointStartAI = {X:superGenius.center.X, Y:superGenius.center.Y};
//                superGenius.way = superGenius.findWay();          
                
//                gameMap.remoteUnitApi.bind("rtest1", striker)


                // Процесс разворота изображения к мышке
                $("#main").mousemove(function(){
//                    striker.setRotateToMouseCoord($("#main"), $("#striker"));
                });

                $(document).mousedown(function(){
//                    var pos = mouseCoordinates();
//                    pos.X += -gameMap.mainEnvirons.marginLeft;
//                    pos.Y += -gameMap.mainEnvirons.marginTop;
//                    Armor["пуколка"].shoot(striker, pos);
                    
                })

                //-------------------------------------------------------------------------------------------------------------------------------------//


                //--------- Обработка движений элемента ------------------------------------------------------------------------------------------------//

                // Заполнение массива клавишь движения
                $(document).keydown(function(event){

                    // Значение нажатой клавиши
                    var activeKey = String.fromCharCode(event.keyCode);

                    // Проверка нажатия нескольких клавишь клавиатуры только для WASD(движения)
                    if(activeKey === "W" || activeKey === "S" || activeKey === "A" || activeKey === "D"){


                        if(keyActiveArray[0] !== activeKey && keyActiveArray[1] !== activeKey){
                            // Добавление зажатых клавишь клавиатуры в массив
                            if(keyActiveArray.length < 2){
                                keyActiveArray.push(activeKey);
                            }
                            // Смена нажатых клавишь клавиатуры
                            else{
                                // Удаление первой зажатой кнопки
                                keyActiveArray.shift();
                                // Добавление второй кнопки
                                keyActiveArray.push(activeKey);
                            }
                        }

                        // Функция движения striker
                        //stepObject(striker);
                    }
                })

                // Работа при отжатии клавишь клавиатуры
                $(document).keyup(function(event){

                    // Вспомогательный элемент для хранения удаленного значения массива зажатых клавишь
                    var deleteElement = 0;

                    // Значение отжатой клавиши
                    var activeKey = String.fromCharCode(event.keyCode);

                    if(activeKey === "W" || activeKey === "S" || activeKey === "A" || activeKey === "D"){
                        for(var i = 0; i < keyActiveArray.length; i++){
                            if(keyActiveArray[i] === activeKey){
                                keyActiveArray.splice(i, 1);
                                deleteElement = i;
                            }
                        }
                        // Данный код необходим, так так при отключении клавиши, сбрасывается событие keyDown
                        if(deleteElement === 1){


                            var timerForStopPress = setInterval(HI, 50);

                            function HI(){
                                // Функция движения striker

                                //stepObject(striker);
                                if(keyActiveArray.length === 0){
                                    clearInterval(timerForStopPress);
                                }
                            }
                        }
                    }
                });
                //-------------------------------------------------------------------------------------------------------------------------------------------//

            });


        //----------------------------------------------------------------------------------------------------------------------------------------------------//



        </script>
        <link rel="stylesheet" type="text/css" href="style/mainStyle.css" />
        <title>Игра всех времен и народов</title>
    </head>
    <body>
        <div id="environs" ><div id="main" style="background-image:url(img/map2.jpg);" ></div></div>
    </body>
</html>
