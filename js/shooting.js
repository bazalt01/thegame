Armor = {}; // Ассоциативный массив "Вооружение"

/**
 * Класс Оружие описывает характеристики оружия
 * @param {string} name         - Имя оружия
 * @param {number} accuracy     - Разброс (максимальный угол)
 * @param {number} rate         - Скорострельность (количество выстрелов в минуту)
 * @param {number} countSpots   - Количество дырок после выстрела
 * @param {number} sizeSpots    - Величина дырок после выстрела
 * @param {number} speedFly     - Скорость полета пули
 * @param --------------------
 * @param {function} shoot      - Выстрел shoot(coordOfElement, coordOfTarget) coordOfElement - координаты стрелявшего, coordOfTarget - координаты цели
 */
function Guns(name, accuracy, rate, countSpots, sizeSpots, speedFly){
    this.name = name;
    this.accuracy = accuracy;
    this.rate = rate;
    this.shootReady = rate;
    this.countSpots = countSpots;
    this.sizeSpots = sizeSpots;
    this.speedFly = speedFly;

    var heightStriker = 50;

    this.shoot = function(shooter, coordTarget){
        
        if(!shooter.life()){ // Проверка жив ли объект
            // Организация задержки перед выстрелом
            if((1000/shooter.shootReady) < this.rate){
                this.shootReady++; 
            }
            else{
                var coordElement = shooter.GetCoordinates(); // Получаем координаты точки выстрела

                // Корректируем координыты с учетом размеров стрелка
                coordElement.X = (shooter.center.X); 
                coordElement.Y = (shooter.center.Y);

                var accuracyRad = this.accuracy*Math.PI/180;                                                                        // Угол разброса в радианах
                var distance = Math.pow(Math.pow((coordTarget.X-coordElement.X),2)+Math.pow((coordTarget.Y-coordElement.Y),2),0.5); // Расстояние до цели
                var radius = Math.pow(Math.pow(distance*Math.tan(accuracyRad), 2), 0.5);                                            // Радиус поражения

                // Поиск радиуса углового разброса на поверхности
                var angleGunfoot = Math.atan(distance/heightStriker);                               // Угол наклона оружия к поверхности
                var radiusDestWA = heightStriker*Math.tan((angleGunfoot+accuracyRad))-distance;     // Дальний радиус поражения на поверхности по длине
                var radiusDestWB = distance-heightStriker*Math.tan((angleGunfoot-accuracyRad));     // Ближний радиус поражения на поверхности по длине
                var radiusDestB = radius;                                                           // Радиус поражения на поверхности по ширине

                var traectory = new Spots("traectory", "traectory",""); // Объект траектория пути               
                var spot = new Spots("spot", "spot","");                // Объект дырка от пули на поверхности

                // Получение координат дырок
                for(var i = 0; i < this.countSpots; i++){
                    var positionX = getRandomInt(-radiusDestB, radiusDestB);                                                        // Координата отверстия по Х в относительной системе координат
                    var kA = (radiusDestB)/(radiusDestWA);                                                                          // Коэффициент верхней полуоси
                    var kB = (radiusDestB)/(radiusDestWB);                                                                          // Коэффициент нижней полуоси
                    var otstupYA = Math.pow(positionX/(kA*20), 2);
                    var otstupYB = Math.pow(positionX/(kB*20), 2);
                    var positionY = getRandomInt(-(radiusDestWB-otstupYB), Math.pow(Math.pow(radiusDestWA-otstupYA, 2),0.5));       // Координата отверстия по Y в относительной системе координат
                    var rx = distance+positionY;
                    var ry = positionX;
                    var a = coordTarget.X-coordElement.X;
                    var alfa = Math.acos(a/distance);
                    if(coordTarget.Y < coordElement.Y){alfa*=-1;}
                    var s1 = Math.cos(alfa);
                    var s2 = Math.sin(alfa);
                    var positionXRotate = coordElement.X + rx * s1 - ry * s2;   // Координата дырки по Х
                    var positionYRotate = coordElement.Y + rx * s2 + ry * s1;   // Координата дырки по Y

                    // Задание координат и размеров траектории
                    if(coordElement.X < positionXRotate && coordElement.Y < positionYRotate){
                        traectory.addCoordinates(coordElement.X, coordElement.Y);
                        traectory.addSize(positionXRotate-coordElement.X, positionYRotate-coordElement.Y);
                    }
                    if(coordElement.X < positionXRotate && coordElement.Y > positionYRotate){
                        traectory.addCoordinates(coordElement.X, positionYRotate);
                        traectory.addSize(positionXRotate-coordElement.X, coordElement.Y-positionYRotate);
                    }
                    if(coordElement.X > positionXRotate && coordElement.Y > positionYRotate){
                        traectory.addCoordinates(positionXRotate, positionYRotate);
                        traectory.addSize(coordElement.X-positionXRotate, coordElement.Y-positionYRotate);
                    }
                    if(coordElement.X > positionXRotate && coordElement.Y < positionYRotate){
                        traectory.addCoordinates(positionXRotate, coordElement.Y);
                        traectory.addSize(coordElement.X-positionXRotate, positionYRotate-coordElement.Y);
                    }

                    // Задание координат и размеров дырки от пули
                    spot.addCoordinates(positionXRotate, positionYRotate);
                    spot.addSize(1, 1);

                    // Функция поиска пересечения пули с объектами
                    function pereborElementsOfSector(traectory, sectors, shooter, spot){

                        var controlElements = [];
                        var spotsArray = []; // Хранит точки попадания в границы объетов
                        var spotsWightArray = []; // Хранит точки попадания в живые объеты
                        //
                        // Перебор всех секторов
                        for(var i in sectors){
                            var sector = gameMap.getSector(sectors[i].i, sectors[i].j); // Получение каждого отдельного сектора

                            // Перебор элементов сектора
                            for(var j in sector.elements){

                                if(!searchForMatches(controlElements, sector.elements[j])){ // Проверка на наличие повторного элемента

                                    controlElements.push(sector.elements[j]); // Если в массиве controlElements нет совпадающего элемента то он добавляется

                                    if(sector.elements[j].classObject == "element"){ // Если есть попадание в объект в пространстве кроме других стрелков

                                        if(sector.elements[j].acrossObject(traectory)){

                                            if(!boundsCheckingEnvirons(traectory)){ // Проверка превосходит ли 
                                                var newSpot = new Spots("newSpot", "newSpot","");   // Объект пересечения траектории с границей объекта
                                                newSpot.addSize(1, 1);                                         
                                                edLineAcross(spot.coordinates,shooter.center,{X:sector.elements[j].coordinates.X, Y:0}, spotsArray, sector.elements[j], newSpot, traectory);                            // Проверка пересечения с левой стороной
                                                edLineAcross(spot.coordinates,shooter.center,{X:sector.elements[j].coordinates.X+sector.elements[j].size.W, Y:0}, spotsArray, sector.elements[j], newSpot, traectory);  // Проверка пересечения с правой стороной
                                                edLineAcross(spot.coordinates,shooter.center,{X:0, Y:sector.elements[j].coordinates.Y}, spotsArray, sector.elements[j], newSpot, traectory);                            // Проверка пересечения с нижней стороной
                                                edLineAcross(spot.coordinates,shooter.center,{X:0, Y:sector.elements[j].coordinates.Y+sector.elements[j].size.H}, spotsArray, sector.elements[j], newSpot, traectory);  // Проверка пересечения с верхней стороной                               

                                            }
                                        }
                                    }
                                    // Если пуля попадает в другого стрелка у него онимается жизнь
                                    if(sector.elements[j].classObject != "element" && sector.elements[j].classObject != shooter.classObject && sector.elements[j] != shooter){
                                        if(!boundsCheckingEnvirons(traectory)){ // Проверка превосходит ли 
                                            var newSpot = new Spots("newSpot", "newSpot","");   // Объект пересечения траектории с границей объекта
                                            newSpot.addSize(1, 1);                                         
                                            edLineAcross(spot.coordinates,shooter.center,{X:sector.elements[j].coordinates.X, Y:0}, spotsWightArray, sector.elements[j], newSpot, traectory);                            // Проверка пересечения с левой стороной
                                            edLineAcross(spot.coordinates,shooter.center,{X:sector.elements[j].coordinates.X+sector.elements[j].size.W, Y:0}, spotsWightArray, sector.elements[j], newSpot, traectory);  // Проверка пересечения с правой стороной
                                            edLineAcross(spot.coordinates,shooter.center,{X:0, Y:sector.elements[j].coordinates.Y}, spotsWightArray, sector.elements[j], newSpot, traectory);                            // Проверка пересечения с нижней стороной
                                            edLineAcross(spot.coordinates,shooter.center,{X:0, Y:sector.elements[j].coordinates.Y+sector.elements[j].size.H}, spotsWightArray, sector.elements[j], newSpot, traectory);  // Проверка пересечения с верхней стороной                               
                                        }
                                    } 
                                }
                            }
                        }
                        
                        var mixlength = 100000000;              // Условное максимальное значение
                        var mixlengthShoot = spotsArray[0];     // Условное ближайшее пересечение с границей
                        // Если есть хоть одна точка пересечений
                        if(spotsArray.length > 0){
                            
                            // Перебираем все пересечения с границами внутри объекта
                            for(var i in spotsArray){
                                var lengthShoot = Math.pow(Math.pow((spotsArray[i].X-shooter.coordinates.X),2)+Math.pow((spotsArray[i].Y-shooter.coordinates.Y),2),0.5);
                                if(lengthShoot < mixlength){ 
                                    mixlength = lengthShoot;
                                    mixlengthShoot = spotsArray[i];
                                }
                            }
                            return mixlengthShoot; // Возвращаем ближайшее значение
                        }
                        else if(spotsWightArray.length > 0){
                            if(typeof(spotsWightArray[0].link.health) != undefined){
                                var lengthShoot = Math.pow(Math.pow((spotsWightArray[0].X-shooter.coordinates.X),2)+Math.pow((spotsWightArray[0].Y-shooter.coordinates.Y),2),0.5);
                                if(lengthShoot < mixlength){
                                    spotsWightArray[0].link.health-=1;
                                }
                            }    
                            
                        }
                    }

                    var acrossSectors = gameMap.acrossSectors(traectory); // Получение секторов на траектории пули

                    // Если пересечения с объектами отсутствуют то идет отрисовка пули
                    var acrossSpot = pereborElementsOfSector(traectory, acrossSectors, shooter, spot); 
                    if(typeof(acrossSpot) != 'object'){
                        var element = document.createElement('div');           // Создание нового элемента div
                        var numberTrash = gameMap.getNumberTrash();
                        gameMap.addTrash("trash"+numberTrash);
                        element.id = "trash"+numberTrash;
                        element.style.left = positionXRotate;
                        element.style.top = positionYRotate;
                        element.style.width = this.sizeSpots;
                        element.style.height = this.sizeSpots;
                        element.style.borderRadius = "4px";
                        element.style.border = "1px solid black";
                        element.style.position = "absolute";
                        document.getElementById("main").appendChild(element); // Добавление элемента к родителю
                        
                    }
                    // Если пуля попадает в объект то на нем остается след
                    else{

                        var element = document.createElement('div');           // Создание нового элемента div
                        var numberTrash = gameMap.getNumberTrash();
                        gameMap.addTrash("trash"+numberTrash);
                        element.id = "trash"+numberTrash;
                        element.style.left = acrossSpot.X;
                        element.style.top = acrossSpot.Y;
                        element.style.width = 1;
                        element.style.height = 1;
                        element.style.borderRadius = "2px";
                        element.style.border = "1px solid black";
                        element.style.position = "absolute";
                        document.getElementById("main").appendChild(element); // Добавление элемента к родителю
                        
                    }
                // Очистка памяти
                delete spot; 
                delete traectory;
                }
            shooter.shootReady = 0;
            } 
        }
    };
    
//    this.bulletFlay = function(shootBegin, shootEnd){
//      
//        var step = processingStep(shootBegin, shootEnd, this.speedFly); // Получаем координаты смещения            
//        
//    };
}


Armor["пистолет"] = new Guns("пистолет", 3, 60, 1, 2, 20);
Armor["дробовик"] = new Guns("дробовик", 3, 5, 20, 2, 20);
Armor["пуколка"] = new Guns("пуколка", 1, 5, 1, 5, 20);



