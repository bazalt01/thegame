//-------------------------------------------------------------------------------------------------------------------------------------------------//
//--------- Данная библиотека описывает окружающее пространство -----------------------------------------------------------------------------------//
//-------------------------------------------------------------------------------------------------------------------------------------------------//

// Пространство состоит из секторов nxn
// 00     --    0(n-1)
// --     --    --
// (n-1)0 --    (n-1)(n-1)
// Секторы служат для разграничения пространства и облегчения поиска


/**
 * Класс описания единой карты пространства theMap()
 * @param getElementOfMap(id)           - Получает объект из всего пространства. Если ID не указано, то функция возвращает массив всех объектов
 * @param addElement(id,element)        - Функуция добавления объектов в массив карты пространства. id - ID элемента, element - объект
 * @param {object} sizeEnvirons         - Размеры пространства
 * @param {object} sizeWindow           - Размеры окна
 * @param CreateSectors                 - Функция создает структуру секторов
 */
function theMap(sizeEnvirons, sizeWindow){

    var sizeEnvirons = sizeEnvirons;

    // Массив для хранения элементов на удаление с отсрочкой
    this.trashOfElement = [];
    
    // Счетчик элементов элементов на удаление
    var numberTrash = 0;
    
    /**
     *  Массив карты пространства
     *  Имеет структуру:
     *  element  - объект
     *  sectors  - массив, хранит секторы пространства
     */
    var map = {};
    var mapOfAbsolutlyElement = {};

    /**
     *  Структура sectors
     *  X0           - начальная координата сектора по оси X
     *  Y0           - начальная координата сектора по оси Y
     *  X            - конечная координата сектора по оси X
     *  Y            - конечная координата сектора по оси Y
     *  elements     - массив объектов находящихся в пределах сектора
     */
    var sectors = [];


    this.remoteUnitApi = new unitApi();


    /**
     * Объект пространства DOM
     * element - Сам объект DOM
     * marginTop - отступ сверху
     * marginLeft - отступ слева
     * clipX2 - Координата X1
     * clipY1 - Координата Y1
     * clipX1 - Координата X2
     * clipY2 - Координата Y2
     * fill - Функция подстановки данных в элемент DOM
     */
    this.mainEnvirons = {
        element: "",
        marginTop: 0,
        marginLeft: 0,
        fill: function(){
            this.element.style.marginTop = this.marginTop;
            this.element.style.marginLeft = this.marginLeft;
        }
    };

    /**
     * Пересечение отрезка с объектами на карте
     */
    this.segmentAcross = function(segment, ignore)
    {
        // segmentAcross(segment1, segment2)
        var res = []
        for(var i in sectors)
        {
            for(var j in sectors[i])
            {
                for(var k in sectors[i][j].elements)
                {

                    var ignoreThis = false;
                    for(var q in ignore)
                    {
                        if(sectors[i][j].elements[k].idObject === ignore[q].idObject)
                        {
                            ignoreThis = true;
                            //console.log("ignore[" + ignore[q].idObject + "]:" + ignore[q].name)
                            break;
                        }
                    }

                    if(ignoreThis)
                    {

                        continue;
                    }

                    //console.log("X", sectors[i][j].elements[k])
                    if(sectors[i][j].elements[k].status.mathCord)
                    {
                        for(var w in sectors[i][j].elements[k].status.mathCord)
                        {
                            //console.log("X " + sectors[i][j].elements[k].name + ":"+JSON.stringify(sectors[i][j].elements[k].status.mathCord[w]))
                            p = segmentAcross(segment, sectors[i][j].elements[k].status.mathCord[w])
                            if(p)
                            {
                                res.push(sectors[i][j].elements[k]);
                            }
                        }
                    }
                }
            }
        }
        if(res.length)
        {
            return res
        }
        return false;

    }

    /**
     * Вернёт все элементы по имени класса
     * @param {type} name
     * @returns {theMap.getAllByClass.elem|Array}
     */
    this.getAllByClass = function(nameArr){

        var elem = []; // Массив секторов на которых находится элемент
        for(var i in sectors)
        {
            for(var j in sectors[i])
            {
                for(var k in sectors[i][j].elements)
                {
                    for(var q in nameArr)
                    {
                        if(sectors[i][j].elements[k].classObject == nameArr[q])
                        {
                            elem.push(sectors[i][j].elements[k])
                        }
                    }
                }
            }
        }
        return elem;
    }

    /**
     * Функция разбиения пространства на секторы CreateSectors(count)
     * @param {number} count - ширина/длина
     */
    this.CreateSectors = function(count){
        // Получаем размеры сектора
        var sizeOneSectorW = sizeEnvirons.W/count;
        var sizeOneSectorH = sizeEnvirons.H/count;
        var X0 = 0;
        var Y0 = 0;
        // Заполняем массив секторов с диапозонами
        for(var i = 0; i < count; i++){
            sectors.push(new Array());
            for(var j = 0; j < count; j++){
                // X0 начало сектора по Х, Х конец сектора по Х, Y0 - начало сектора по Y, Y - конец сектора по Y
                sectors[i][j] = {X0: X0, X:Math.floor(sizeOneSectorW*(j+1)), Y0: Y0, Y:Math.floor(sizeOneSectorH*(i+1)), elements: []};
                X0 = Math.floor(sizeOneSectorW*(j+1));
                // если следующий такт последний то округляем
                if(j+2 === count){X = Math.ceil(sizeOneSectorW*(j+1));}
            }
            X0 = 0;
            Y0 = Math.floor(sizeOneSectorH*(i+1));
            // если следующий такт последний то округляем
            if(i+2 === count){X = Math.ceil(sizeOneSectorH*(i+1));}

        }
    };


    /**
     * Функуция добавления объектов в массив карты пространства AddElement(element)
     * @param {object} element   - объект
     */
    this.addElement = function(element){

        // Добавление в массив происходит только при заданном объекте
        if(typeof(element) !== "undefined"){
            var activeSectors = this.acrossSectors(element); // Получаем массив секторов на которых находится элемент
            for(var i in activeSectors){
                sectors[activeSectors[i].i][activeSectors[i].j].elements[element.name] = element; // Добаляем объект
            }
            map[element.name] = {element:element};//{id:id, element:element};
        }
    };
    
    /**
     * Функуция добавления абсолютных объектов (интеллектуальных агентов и т.д.) в массив карты пространства addAbsolutlyElement(element)
     * @param {object} element   - объект
     */
    this.addAbsolutlyElement = function(element){

        // Добавление в массив происходит только при заданном объекте
        if(typeof(element) !== "undefined"){
            mapOfAbsolutlyElement[element.name] = {element:element};//{id:id, element:element};
        }
    };

    /**
     * Функуция удаления объекта из массива карты пространства
     * @param {object} element   - объект
     */
    this.removeElement = function(element){
        if(element !== undefined)
        {
            var activeSectors = this.acrossSectors(element); // Получаем массив секторов на которых находится элемент
            for(var i in activeSectors)
            {
                delete sectors[activeSectors[i].i][activeSectors[i].j].elements[element.name]
            }
            delete map[element.name]
            $(element.element).remove()
        }
    };

    /**
     * Переопределяет принадлежность элемента к секторам
     * @param {array} oldArraySectors - Старый набор секторов
     * @param {array} oldArraySectors - Новый набор секторов
     * @param {object} element - Заменяемый элемент
     */
    this.editElementInSector = function(oldArraySectors, newArraySectors, element){

        // Чистка секторов где был элемент
        for(var i in oldArraySectors){
            delete sectors[oldArraySectors[i].i][oldArraySectors[i].j].elements[element.name];
        }
        // Заполнение элемента в текущие сектора
        for(var i in newArraySectors){
            sectors[newArraySectors[i].i][newArraySectors[i].j].elements[element.name] = element;
        }
    };

    /**
     * Получает объект из всего пространства. Если ID не указано, то функция возвращает массив всех объектов - getElementOfMap(id)
     * @param {string} id - ID объекта
     */
    this.getElementOfMap = function(id){
        if(typeof(id) !== 'undefined' && typeof(id) !== ''){
            for(var elem in map){
                if(map[elem].element.id === id){return map[elem].element;} // Возвращаем объект по ID
            }
        }
        else{return map;} // Возвращаем массив
    };

    /**
     * getSizeEnvirons - возвращает размеры пространства
     */
    this.getSizeEnvirons = function(){
        return sizeEnvirons;
    };

    /**
     * getSizeWindow - возвращает размеры окна
     */
    this.getSizeWindow = function(){
        return sizeWindow;
    };

    /**
     * getSector - Функция возвращает сектор при заданных параметра его расположения или все сектора если параметры отсутствуют
     * @param {number} i - номер строки
     * @param {number} j - номер столбца
     */
    this.getSector = function(i,j){
        if((typeof(i) !== 'undefined' && typeof(i) !== '') && (typeof(j) !== 'undefined' && typeof(j) !== '')){return sectors[i][j];} // Возвращает конкретный сектор если параметры заданы
        else{return sectors;} // Возвращает массив секторов
    };


    /**
     * Поиск секторов которые пересекает объект "across_object"
     * @param {object} across_object - проверяемый объект
     */
    this.acrossSectors = function(across_object){
        var activeSectors = []; // Массив секторов на которых находится элемент
        for(var i in sectors){
            for(var j in sectors[i]){
                var sector = new ObjectOfEnvirons("sector", "sector", "");
                sector.addCoordinates(sectors[i][j].X0, sectors[i][j].Y0);
                sector.addSize(sectors[i][j].X-sectors[i][j].X0, sectors[i][j].Y-sectors[i][j].Y0);
                if(sector.acrossObject(across_object)){
                    activeSectors.push({i:i, j:j}); // Добавляем координаты сектора
                }
                delete sector; // очистка памяти
            }
        }
        return activeSectors;
    };
    
    /**
     * Функция добавляет елемент на удаление с пространства
     * @param {string} ID   - элемента
     * @param {number} time - значение задержки в секундах
     */
    this.addTrash = function(elem, time){
        if(typeof(time) == "undefined"){
            this.trashOfElement.push({id: elem, time: 200}); // При отчуствии значения time значение ставится по умолчанию 200 (10 секунд)
        }
        else{this.trashOfElement.push({id: elem, time: time*20});}
    };
    
    /**
     * Функция увеличивает значение счетчика и возвращает его значение
     * @returns {number} numberTrash - возвращает значение счетчика улементов на удаление
     */
    this.getNumberTrash = function(){
        numberTrash++;
        return numberTrash;
    }

    /**
     * Главный игровой цикл
     * @returns {undefined}
     */
    this.StartMainLoop = function()
    {
        this.time = 0;
        var thisObj = this
        function mainLoop()
        {
            thisObj.time += 1;
            for(var i in map)
            {
                if(map[i].element && map[i].element.TimeTiсk)
                {
                    //console.log(map[i].element.name, keyActiveArray)
                    map[i].element.TimeTiсk(thisObj.time);
                }
            }
            
            for(var i in mapOfAbsolutlyElement)
            {
                if(mapOfAbsolutlyElement[i].element && mapOfAbsolutlyElement[i].element.TimeTiсk)
                {
                    //console.log(map[i].element.name, keyActiveArray)
                    mapOfAbsolutlyElement[i].element.TimeTiсk(thisObj.time);
                }
            }
            
            

            if(thisObj.remoteUnitApi)
            {
                thisObj.remoteUnitApi.TimeTiсk(thisObj.time);
            }
            for(var i in gameMap.trashOfElement){
                if(gameMap.trashOfElement[i].time <= 0){
                    var elem = document.getElementById(gameMap.trashOfElement[i].id);
                    var parent = document.getElementById("main");
                    parent.removeChild(elem);
                    gameMap.trashOfElement.splice(i,1);
                }
                else{ gameMap.trashOfElement[i].time--;}
            }
            setTimeout(mainLoop, 50)
        }
        
        mainLoop();
    }
    
}
