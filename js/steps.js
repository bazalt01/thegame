/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/** 
 * Функция возвращает массив координат мышки относительно страницы mouseCoordinates(event)
 * @param event - событие
 */
function mouseCoordinates(event){
    event = event || window.event;
    if(event.pageX == null && event.clientX != null){      
        event.pageX = event.clientX;
        event.pageY = event.clientY;
    }           
    return {X: event.pageX, Y: event.pageY};
}
   
