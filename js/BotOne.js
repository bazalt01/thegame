/*
 * Класс управлямых удалённо объектов
 */


BotOne = function(name, id_object, class_object)
{
    BotOne.superclass.constructor.call(this, name, id_object, class_object);
    extend(this, Wight);

    this.health = 100

    this.status.LastSlowBulletFire = 0;
    this.status.SlowBulletFireSpeed = 4000;


    this.lastRes_dAngle = 999;
    this.curentGoal = undefined

    this.enemies=[]

    this.nextPosition = undefined
    this.groupPos = undefined
    this.groupBot = []

    var thisObj = this
    setTimeout(function(){
        var group = gameMap.getAllByClass([class_object])
        if(group && group.length > 0 )
        {
            thisObj.groupBot = group;
            console.log("Образована группа из "+class_object+"" , thisObj.groupBot )
        }
        else
        {
            console.log("Группа из "+class_object+" не удалась" , group)
        }
    }, 1000)
}

    /**
     * Массив с именами классов врагов.
     */
    BotOne.prototype.setEnemies = function( enemies )
    {
        this.enemies = enemies;
    }

    BotOne.prototype.goTo = function( point )
    {
        if(!point)
        {
            return;
        }
        var r = this.GetAngleTo( point );
        
        var v = normVector({X: point.X - this.coordinates.X, Y:point.Y - this.coordinates.Y})
        

        //testPoint({X:point.X,Y:point.Y}, "F0F")
        this.step(v)
    }

    BotOne.prototype.getDist = function( elem )
    {
        return lenVector({X: this.coordinates.X - elem.coordinates.X, Y: this.coordinates.Y - elem.coordinates.Y })
    }

    BotOne.prototype.TimeTiсk = function( time )
    {
        
        if(time% 50 == 0)
        {
            this.FactorX = Math.random() * Math.PI*2
        }
        
        if(this.health < 0)
        {
            gameMap.removeElement(this);
        }

        this.curentGoal = this.getNewGoal(undefined)
        if(this.curentGoal && this.curentGoal.health > 0)
        {
            // Ииеем цель
            var angle = this.GetAngleTo( this.curentGoal.coordinates )
            this.rotateStep( toDeg(angle) );

            if( Math.abs(toDeg(angle)) < 10)
            {
                this.SlowBulletFire();
            }
            
            this.nextPosition =  this.curentGoal.coordinates
        }
        else if(this.groupBot.length > 0  )
        {
            if(this.groupBot[0].idObject == this.idObject)
            {// Лидер группы

                var maxLen = 0;
                var maxLenPoint;
                var geenPoint = [];
                var RadLen = 100;
                for(var alpha = this.FactorX; alpha < Math.PI*2; alpha+= Math.PI/8 + this.FactorX)
                {
                    var x = this.coordinates.X + Math.cos(alpha)*RadLen;
                    var y = this.coordinates.Y + Math.sin(alpha)*RadLen;

                    //testPoint({x:x, y:y})

                    var res = gameMap.segmentAcross({x1: this.coordinates.X, y1: this.coordinates.Y, x2: x, y2: y },this.groupBot)
                    if(res)
                    {
                        var minLen = 9999;
                        var minPoint;
                        var arrLen = []
                        for(var k in res)
                        {
                            for(var i in res[k].status.mathCord) // Находим близжайшею точку пересечения с объектом
                            {
                                var rPoint = segmentAcross({x1: this.coordinates.X, y1: this.coordinates.Y, x2: x, y2: y },res[k].status.mathCord[i])
                                if(rPoint)
                                { 
                                    var rLen = lenVector({X:this.coordinates.X - rPoint.X, Y:this.coordinates.Y - rPoint.Y})
                                    arrLen.push(rLen)
                                    if(minLen > rLen)
                                    {
                                        minLen = rLen;
                                        minPoint = rPoint;
                                        //testPoint(rPoint, "7f7")
                                    }
                                }
                            }
                        }

                        if(minPoint && maxLen < RadLen)
                        {
                            
                            maxLen = minLen;
                            maxLenPoint = minPoint;
                            //testPoint(minPoint, "7f7")
                        }
                    }
                    else if(maxLen < RadLen)
                    {
                        maxLen = RadLen;
                        maxLenPoint = {X:x, Y:y};
                        //testPoint(maxLenPoint, "7f7")
                    }
 
                     //testPoint(maxLenPoint, "ccc")
                }
  
                this.nextPosition = maxLenPoint;
                //testPoint(this.nextPosition, "00f")
            }
            else
            {// Член группы
                if(this.getDist(this.groupBot[0]) > 50)
                {
                    this.nextPosition = this.groupBot[0].coordinates
                }
                else
                {
                    this.nextPosition = false
                }
            }
        }
 
        this.goTo(this.nextPosition)
        


    }

    BotOne.prototype.getNewGoal = function(GoalArr, adnTrue)
    {
        if(!GoalArr)
        {
            var GoalArr = gameMap.getAllByClass(this.enemies)
            if(!GoalArr || GoalArr.length == 0)
            {
                return undefined
            }

        }

        //console.log("Поиск цели из " + GoalArr.length ) // Цель спрятана за препятсвием
        var Res_dA = 999;
        var TmplastGoal = undefined;
        for(var i in GoalArr)
        {
            var angle = this.GetAngleTo( GoalArr[i].coordinates )
            var dA = toDeg(angle);
            
            var dA = lenVector({X:GoalArr[i].coordinates.X - this.coordinates.X, Y:GoalArr[i].coordinates.Y - this.coordinates.Y});

            res = gameMap.segmentAcross({x1: GoalArr[i].coordinates.X, y1: GoalArr[i].coordinates.Y, x2: this.coordinates.X, y2: this.coordinates.Y },[this, GoalArr[i]])
            if(res /*|| adnTrue*/)
            {
                // console.log("segmentAcross["+GoalArr[i].name+"]",  res) // Цель спрятана за препятсвием
//                console.log("Цель " + GoalArr[i].name )
//                if(Res_dA > dA || Res_dA > 400)
//                {
//                    Res_dA = dA;
//                    TmplastGoal = GoalArr[i];
//                }
            }
            else
            {
                //console.log("Цель " + GoalArr[i].name )
                if(Res_dA > dA || Res_dA > 400)
                {
                    Res_dA = dA;
                    TmplastGoal = GoalArr[i];
                }
            }
        }
        
        //console.log("Цель " + Res_dA + "\t" + GoalArr[i].name )
        this.lastAngleToGoal = Res_dA;
        return TmplastGoal;
    }

    BotOne.prototype.SlowBulletFire = function(  )
    {
        var curent = new Date().getTime()
        if(curent - this.status.LastSlowBulletFire > this.status.SlowBulletFireSpeed)
        {
            this.status.LastSlowBulletFire = curent
            var bullet = new SlowBullet(this);
            return true;
        }

        return false;
    }

    /**
     * Выводит координаты объекта внутри него самого (Для отладки)
     */
    BotOne.prototype.visibleCoord = function()
    {
        this.element.innerHTML = "<br><br>"+this.name+"<br>"+this.health;
    };

    extend(BotOne, Wight);