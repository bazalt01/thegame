
/**
 * Универсальный класс Объектов пространства - ObjectOfEnvirons(name, id_object, class_object)
 * @param {string} name             - имя объекта
 * @param {object} coordinates      - координаты объекта
 * @param {string} id_object        - ID объекта
 * @param {string} class_object     - класс объекта
 * @param {object} size             - размеры объекта
 * @param {DOM} element             - DOM элемент
 * @param {array} sector            - массив секторов в которых распологается элемент
 * @param -----------
 * @param {function} createElement(parent)         - Создание и добавления div элемента на пространство. parent - id пространства
 * @param {function} acrossObject(across_object)   - Проверка на нахождение объекта. "across_object" в текущем объекте - across_object
 * @param {function} visibleCoord()                - Выводит координаты объекта внутри него самого (Для отладки)
 * @param {function} addClass(class_name)          - Присваивает класс объекту
 * @param {function} addSize(width,height)         - Задает размеры объекта
 * @param {function} addCoordinates(x,y)           - Задает координаты
 */
ObjectOfEnvirons = function(name, id_object, class_object){
    this.name = name;
    this.coordinates = {X: 0, Y: 0};
    this.idObject = id_object;
    this.classObject = class_object;
    this.size = {W: 0, H: 0};  // Лучше масив для описания объекта любой сложности.
    this.element;
    this.sector = []; // 
    this.HTML = ""; // 
    this.center = {X: 0, Y: 0};

    /**
     * Состояние объекта
     */
    this.status = {};
    this.status.rotate = 0;
    this.status.weight = 1; // Вес
    this.status.mathCord = []; // Задаётся как массив отрезков {x1:0,y1:0,x2:1,y2:1}
    
    
    return this;
}


    /**
     * Создание и добавления div элемента на пространство - createElement(parent)
     * @param {string} parent - id пространства
     */
    ObjectOfEnvirons.prototype.createElement = function(parent){
        this.element = document.createElement('div');               // Создание нового элемента div
        this.element.id = this.idObject;                            // Присвоение ID
        
        if(parent !== undefined)
        {
            document.getElementById(parent).appendChild(this.element);// Добавление элемента к родителю
        }  
        else
        {
            gameMap.mainEnvirons.element.appendChild(this.element);// Добавление элемента к родителю
        }
        
        gameMap.addElement(this);                                   // Занесение объекта в карту пространства
    };

    /**
     * Проверка на нахождение объекта в текущем объекте - across_object(across_object)
     * @param {object} across_object - объект пересечения
     * @global gameMap
     */
    ObjectOfEnvirons.prototype.acrossObject = function(across_object){
        var getSizeEnvirons = gameMap.getSizeEnvirons();        
        if(
                // Левый верхний угол объекта
                ((across_object.coordinates.X >= this.coordinates.X && across_object.coordinates.X <= this.coordinates.X+this.size.W) &&
                (across_object.coordinates.Y >= this.coordinates.Y && across_object.coordinates.Y <= this.coordinates.Y+this.size.H)) ||
                // Правый нижний угол объекта
                ((across_object.coordinates.X+across_object.size.W >= this.coordinates.X && across_object.coordinates.X+across_object.size.W <= this.coordinates.X+this.size.W) &&
                (across_object.coordinates.Y >= this.coordinates.Y && across_object.coordinates.Y <= this.coordinates.Y+this.size.H)) ||
                // Левый нижний угол объекта
                ((across_object.coordinates.X+across_object.size.W >= this.coordinates.X && across_object.coordinates.X+across_object.size.W <= this.coordinates.X+this.size.W) &&
                (across_object.coordinates.Y+across_object.size.H >= this.coordinates.Y && across_object.coordinates.Y+across_object.size.H <= this.coordinates.Y+this.size.H)) ||
                // Правый верхний угол объекта
                ((across_object.coordinates.X >= this.coordinates.X && across_object.coordinates.X <= this.coordinates.X+this.size.W) &&
                (across_object.coordinates.Y+across_object.size.H >= this.coordinates.Y && across_object.coordinates.Y+across_object.size.H <= this.coordinates.Y+this.size.H)) ||
                // Объект пересекает по горизонтали
                ((across_object.coordinates.X <= this.coordinates.X && across_object.coordinates.X+across_object.size.W >= this.coordinates.X+this.size.W) &&
                (across_object.coordinates.Y >= this.coordinates.Y && across_object.coordinates.Y <= this.coordinates.Y+this.size.H)) ||
                ((across_object.coordinates.X <= this.coordinates.X && across_object.coordinates.X+across_object.size.W >= this.coordinates.X+this.size.W) &&
                (across_object.coordinates.Y+across_object.size.H >= this.coordinates.Y && across_object.coordinates.Y+across_object.size.H <= this.coordinates.Y+this.size.H)) ||
                // Объект пересекает по вертикали
                ((across_object.coordinates.X >= this.coordinates.X && across_object.coordinates.X <= this.coordinates.X+this.size.W) &&
                (across_object.coordinates.Y <= this.coordinates.Y && across_object.coordinates.Y+across_object.size.H >= this.coordinates.Y+this.size.H)) ||
                ((across_object.coordinates.X+across_object.size.W >= this.coordinates.X && across_object.coordinates.X+across_object.size.W <= this.coordinates.X+this.size.W) &&
                (across_object.coordinates.Y <= this.coordinates.Y && across_object.coordinates.Y+across_object.size.H >= this.coordinates.Y+this.size.H)) ||
                // Объект полностью покрывает
                ((across_object.coordinates.X <= this.coordinates.X && across_object.coordinates.X+across_object.size.W >= this.coordinates.X+this.size.W) &&
                (across_object.coordinates.Y <= this.coordinates.Y && across_object.coordinates.Y+across_object.size.H >= this.coordinates.Y+this.size.H)) ||
                // проверка на пересечение с границами пространства
                (across_object.coordinates.X < 0 || across_object.coordinates.X+across_object.size.W > getSizeEnvirons.W || across_object.coordinates.Y < 0 || across_object.coordinates.Y+across_object.size.H > getSizeEnvirons.H)


          ){
                return true; // Проверяемый объект пересекает текущий
        }
        else{return false;} // Объекты не пересекаются

    };

    /**
     * Добавление оформление объекта - addStyle(param, value). При отсутствии параметров задает оформление по умолчанию
     * @param {string} param - Атрибут CSS
     * @param {string} value - Значение
     */
     ObjectOfEnvirons.prototype.addStyle = function(param, value){
        // При отсутствии праметров функции устанавливается стандартные значения стиля
        if((typeof(param) === 'undefined' || param === "") && (typeof(value) === 'undefined' || value === "")){

            this.element.style.position = "absolute";                   // Задается позиция абсолют

            // Задаются координаты объекта
            this.element.style.left = this.coordinates.X+"px";
            this.element.style.top = this.coordinates.Y+"px";

            // Задаются размеры
            this.element.style.width = this.size.W;
            this.element.style.height = this.size.H;

            this.element.style.border = "1px solid black";  // Довление обрамления
        }
        else{
            this.element.style[param] = value;
       }
    };

    /**
     * Выводит координаты объекта внутри него самого (Для отладки) - visibleCoord()
     */
    ObjectOfEnvirons.prototype.visibleCoord = function(){
        this.element.innerHTML = this.HTML+"X:"+this.coordinates.X+" Y:"+this.coordinates.Y+"</br>"+this.name;
    };

    /**
     * Присваивает класс объекту - addClass(class_name). При отсутствии значения ставиться класс по умолчанию
     * @param {string} class_name - Имя класса
     */
    ObjectOfEnvirons.prototype.addClass = function(class_name){

        // Если класс не задан то присваивается по умолчанию, иначе заданный в функции
        if(typeof(class_name) === 'undefined'){
            this.element.className = this.classObject; // по умолчанию
        }
        else{this.element.className = class_name;} // заданный
    };

    /**
     * Задает размеры объекта - addSize(width, height)
     * @param {number} width     - ширина объекта
     * @param {number} height    - высота объекта
     */
    ObjectOfEnvirons.prototype.addSize = function(width, height){
        this.size.W = width;
        this.size.H = height;
        this.center.X = this.coordinates.X+width/2;
        this.center.Y = this.coordinates.Y+height/2;
    };
    
    ObjectOfEnvirons.prototype.getSize = function(){
        return {w:this.size.W , h:this.size.H}
    };

    /**
     * Задает координаты - addCoordinates(x, y)
     * @param {number} x     - координата x
     * @param {number} y     - координата y
     */
    ObjectOfEnvirons.prototype.addCoordinates = function(x, y){
        this.coordinates.X = x;
        this.coordinates.Y = y;
    };
    
    /**
     * 
     * @param {array} cord Масив отрезков {x1:350, y1:100, x2:450, y2:200} 
     * @returns {Boolean}
     */
    ObjectOfEnvirons.prototype.addMathCord = function(cord){
        if(cord)
        {
            this.status.mathCord = cord;
            
        }
        else
        {
            this.status.mathCord = []
            this.status.mathCord.push({
                x1:this.coordinates.X,
                y1:this.coordinates.Y,
                x2:this.coordinates.X + this.size.W,
                y2:this.coordinates.Y
            })

            this.status.mathCord.push({
                x1:this.coordinates.X,
                y1:this.coordinates.Y,
                x2:this.coordinates.X,
                y2:this.coordinates.Y + this.size.H
            })

            this.status.mathCord.push({
                x1:this.coordinates.X,
                y1:this.coordinates.Y + this.size.H,
                x2:this.coordinates.X + this.size.W,
                y2:this.coordinates.Y + this.size.H
            })

            this.status.mathCord.push({
                x1:this.coordinates.X + this.size.W,
                y1:this.coordinates.Y,
                x2:this.coordinates.X + this.size.W,
                y2:this.coordinates.Y + this.size.H
            })
        }
        
        for(var i in this.status.mathCord)
        {
            testPoint(this.status.mathCord[i]);
        }
    };
    
    /**
     * Возвращает координаты - GetCoordinates()
     */
    ObjectOfEnvirons.prototype.GetCoordinates = function(){
        return {X:this.coordinates.X, Y:this.coordinates.Y};
    }
    
    
    /**
     * Должна вызыватся когда какой либо объект столкнётся с этим объектом.
     * @param {number} element     - элемент с которым произошло столкновение
     */
    ObjectOfEnvirons.prototype.onAcross = function(element){
        
    }