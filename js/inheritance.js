//-------------------------------------------------------------------------------------------------------------------------------------------------//
//--------- Библиотека наследования ---------------------------------------------------------------------------------------------------------------//
//-------------------------------------------------------------------------------------------------------------------------------------------------//



// копирует все свойства из src в dst,
// включая те, что в цепочке прототипов src до Object
function mixin(dst, src){
    // tobj - вспомогательный объект для фильтрации свойств,
    // которые есть у объекта Object и его прототипа
    var tobj = {}
    for(var x in src){
        // копируем в dst свойства src, кроме тех, которые унаследованы от Object
        if((typeof tobj[x] == "undefined") || tobj[x] != src[x]){
            dst[x] = src[x];
        }
    }
    
    // В IE пользовательский метод toString отсутствует в for..in
    if(document.all && !document.isOpera){
        var p = src.toString;
        if(typeof p == "function" && p != dst.toString && p != tobj.toString && p != "\nfunction toString() {\n    [native code]\n}\n")
        {
            dst.toString = src.toString;
        }
    }
}



/** 
* Функция наследования - extend(Child, Parent)
* @param Child  - класс наследник
* @param Parent - класс родитель
*/
function extend(Child, Parent, name) {
   wChild = Child;
   wParent = Parent;
   wName = name;
   
    //     console.log("extend", name)
    for(var x in Parent.prototype){
        // копируем в dst свойства src, кроме тех, которые унаследованы от Object
        if(Parent.prototype[x] !== undefined && Parent.prototype[x] !== Child[x] && x !== "prototype" && x !== "__proto__" &&  Child[x] === undefined)
        {
            if(Child.prototype === undefined || Child.prototype[x] === undefined )
            {
                Child.prototype[x] = Parent.prototype[x];
            }    
            
          //  console.log("prototype", x)
        }
    }
    
    
    //var F = function() { }
    //F.prototype = Parent.prototype;
    //Child.prototype = new F();
    Child.constructor = Child;
    Child.superclass = Parent.prototype;
          //  console.log("~~~~~~~~~extend~~~~~~~~~")
}





