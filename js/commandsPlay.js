//-------------------------------------------------------------------------------------------------------------------------------------------------//
//--------- Опысание нажатия на кнопки и дейставия ------------------------------------------------------------------------------------------------//
//-------------------------------------------------------------------------------------------------------------------------------------------------//

var commandsMoveKey = {
    
    _STOP: function(elem){    
        var speed = {X: 0, Y:0};
        if(elem.step) elem.step(speed);
    },
    W: function(elem){    
        var speed = {X: 0, Y:-2};
        if(elem.step) elem.step(speed);
    },
    
    S: function(elem){
        var speed = {X: 0, Y:2};
        if(elem.step) elem.step(speed);
    },
    
    A: function(elem){
        var speed = {X: -2, Y:0};
        if(elem.step) elem.step(speed);
    }, 
    
    D: function(elem){
        var speed = {X: 2, Y:0};
        if(elem.step) elem.step(speed);
    },   
    
    WA: function(elem){
        var speed = {X: -2, Y:-2};
        if(elem.step) elem.step(speed);
    },  
    WD: function(elem){
        var speed = {X: 2, Y:-2};
        if(elem.step) elem.step(speed);
    },
    SA: function(elem){
        var speed = {X: -2, Y:2};
        if(elem.step) elem.step(speed);
    },
    SD: function(elem){
        var speed = {X: 2, Y:2};
        if(elem.step) elem.step(speed);
    }
    
};




// Массив хранения зажатых клавишь WASD
var keyActiveArray = new Array();

