
/**
 * Wight(Существо) описает любой движущийся объект отождествляемый с живым или управляемым
 * @param {string} name
 * @param {string} id_object
 * @param {string} class_object
 */
Wight = function(name, id_object, class_object){
    Wight.superclass.constructor.call(this, name, id_object, class_object);
    extend(this, ObjectOfEnvirons, "this Wight -> ObjectOfEnvirons");
    var mapActiveVertex = [];
    this.health = 100; // Здоровье
    this.HTML = "";
    this.Armor;
    this.curentSpeed = {X:0, Y:0};
    this.deleted = false;
    
    //this.status.pos = {x:0, y:0}
    this.status.speed = this.curentSpeed
    this.status.rotateSpeed = 15 // Максимальная скорость вращения в градусах
    
    this.shootReady = 0; // Задержка выстрела
    
    return this;
};


    Wight.prototype.generateMapActiveVertex = function(map){
        for(var i in map){
            var sizeEnvirons = gameMap.getSizeEnvirons(); // Получаем размеры пространства
            var vertex = []; // Массив рабочих вершин
            // Проверка на соприкосновение с границами пространства
            if(map[i].coordinates.X !== 0 && map[i].coordinates.Y !== 0){vertex.push({X: map[i].coordinates.X, Y: map[i].coordinates.Y});}                                  // Левый верхний угол

            if(map[i].coordinates.X+map[i].size.W !== sizeEnvirons.W && map[i].coordinates.Y !== 0){vertex.push({X: map[i].coordinates.X, Y: map[i].coordinates.Y});}                  // Правый верхний угол

            if(map[i].coordinates.X+map[i].size.W !== sizeEnvirons.W && map[i].coordinates.Y+map[i].size.H !== sizeEnvirons.H){vertex.push({X: map[i].coordinates.X, Y: map[i].coordinates.Y});}  // Правый нижний угол

            if(map[i].coordinates.X+map[i] !== 0 && map[i].coordinates.Y+map[i].size.H !== sizeEnvirons.H){vertex.push({X: map[i].coordinates.X, Y: map[i].coordinates.Y});}           // Левый нижний угол

            // Добавление всех рабочих вершин
            mapActiveVertex.push({element: map[i], vertex: vertex});

        }
    };

    /**
     * Функция перемещения объекта при заданной скорости и пути move(speed, width_way)
     * @param {number} speed        - скорость
     * @param {object} width_way    - пункт назначения
     */
    Wight.prototype.move = function(speed, destination){

        var passedWay = 0;

        var elem = this; // Сам объект
        // Вычисление шага по отношении к оси Х
        var A = destination.X-this.coordinates.X; // катет а
        var C = Math.pow(Math.pow(destination.X-this.coordinates.X, 2) + Math.pow(destination.Y-this.coordinates.Y, 2), 0.5); // гипотенуза
        var angle = Math.acos(A/C); // Угол между ними в радианах
        var add = Math.acos(0.97/1)*180/Math.PI;

        // Уравнение прямой
        var a = destination.Y-this.coordinates.Y; // коэффициент a
        var b = this.coordinates.X-destination.X; // коэффициент b
        var c = this.coordinates.Y*destination.X-this.coordinates.X*destination.Y; // коэффициент c

        // Перемещение элемента
        function moveElement(elem, speed){
            passedWay+=speed;
            if(passedWay > C){speed = speed-C-passedWay;} // Если длина пройденого пути больше заданного то следующий шаг корректируется
            var X = Math.cos(angle)*speed;
            alert(X);
            var Y = 0;
            Y = (a*(X+elem.coordinates.X)+c)/b;
            Y = Math.pow(Y*Y, 0.5)-elem.coordinates.Y;
            elem.coordinates.X+=X;
            elem.element.style.left = (elem.coordinates.X)+"px";

            elem.coordinates.Y+=Y;
            elem.element.style.top = (elem.coordinates.Y)+"px";
            if(passedWay === C){
                clearInterval(timer);
            }

            // ------------- Отладочная информация --------------------------------------------------------------------------------------------------------------//
            elem.visibleCoord(); // Отображение текущих координат объекта
            // --------------------------------------------------------------------------------------------------------------------------------------------------//
        }
        // Осуществление задержки перед шагом смещения
        var timer = setInterval(function(){moveElement(elem, speed);}, 50);
    };


    /**
     * Выводит координаты объекта внутри него самого (Для отладки) - visibleCoord()
     */
    Wight.prototype.visibleCoord = function(){
        //this.element.innerHTML = "X:"+this.coordinates.X+" Y:"+this.coordinates.Y+"</br>"+this.name+" <br>"+this.health;
        this.element.innerHTML = this.name+" <br>"+this.health;
    };

    /**
     * Разворот объекта 
     * @param float value колво градусов поворота
     * @returns {undefined}
     */
    Wight.prototype.rotate = function(value)
    {
        if(isNaN(value)) return;
        
        this.visibleCoord(); // Отображение текущих координат объекта
        value *= ((2*Math.PI)/360); // Перевод градусов в радианы
        this.status.rotate += value;
        $(this.element).css({transform: 'rotate('+this.status.rotate+'rad)'});
    }
    
    Wight.prototype.rotateStep = function(value)
    {
        if( value > this.status.rotateSpeed) value = this.status.rotateSpeed;
        if( value < -this.status.rotateSpeed) value = -this.status.rotateSpeed;
        this.rotate(value)
    }
    /**
     * Установить разворот объекта 
     * @param float value колво градусов
     * @returns {undefined}
     */
    Wight.prototype.setRotate = function(value)
    {
        value *= ((2*Math.PI)/360); // Перевод градусов в радианы
        this.status.rotate = value;
        $(this.element).css({transform: 'rotate('+this.status.rotate+'rad)'});
    }
    
    Wight.prototype.getRotate = function()
    {
        var value = this.status.rotate;
        value *= 180/Math.PI;
        return value;
    }
    
    /**
     * Выстрел
     * @param {type} len дальность
     * @param {type} gun оружие
     * @returns {undefined}
     */
    Wight.prototype.fire = function(len, gun)
    {
        if(len === undefined)
        {
            len = 100;
        }
        
        var x = this.coordinates.X + (Math.cos(this.status.rotate)*len)
        var y = this.coordinates.Y + (Math.sin(this.status.rotate)*len)
        //console.log(x, y)
        if(gun === undefined)
        {
            Armor["пуколка"].shoot(this, {X:x, Y:y});
        }
        else
        {
            Armor[gun].shoot(this, {X:x, Y:y});
        }
    }
    
    Wight.prototype.getStatus = function(){
        return {
            pos:{
                X:this.coordinates.X, Y: this.coordinates.Y
            },
            speed:this.status.speed,
            rotate:this.status.rotate
        };
    };

    /**
     * Перемещение пошаговое
     * @param {object} speed - скорость перемещения
     * @global gameMap
     */
    Wight.prototype.setPos = function(pos){  
        this.coordinates.X=pos.X;
        this.coordinates.Y=pos.Y; 
        
        this.element.style.left = this.coordinates.X+"px";
        this.element.style.top = this.coordinates.Y+"px";
    };
    
    Wight.prototype.setPosX = function(x){
        this.coordinates.X= x;
        this.element.style.left = this.coordinates.X+"px";
    };
    
    Wight.prototype.setPosY = function(y){
        this.coordinates.Y= y;
        this.element.style.top = this.coordinates.Y+"px";
    };
    
    Wight.prototype.addPosX = function(x){
        this.coordinates.X+= x;
        this.center.X+= x;
        this.element.style.left = this.coordinates.X+"px";
    };
    
    Wight.prototype.addPosY = function(y){
        this.coordinates.Y+= y;
        this.center.Y+= y;
        this.element.style.top = this.coordinates.Y+"px";
    };
     
 
    Wight.prototype.getSpeed = function(){
        return {
            X:this.status.speed.X, 
            Y:this.status.speed.Y
        }
    }

    /**
     * Перемещение пошаговое
     * @param {object} speed - скорость перемещения
     * @global gameMap
     */
    Wight.prototype.step = function(speed, speedY){
        
        if(speedY!== undefined)
        {
            speed = {X:speed, Y:speedY}
        }

        this.status.speed = speed;
        this.curentSpeed = speed; // Для удаления


        var acrossSectors = gameMap.acrossSectors(this); // Получаем список секторов перекающихся с объектом

        // Проверка элемента идет на перед(прогноз), тоесть через элемент testObject
        var forecastX = new ObjectOfEnvirons("test", "test", ""); // Тестовый объект при смещении по X
        forecastX.addCoordinates(this.coordinates.X+speed.X*2, this.coordinates.Y);
        forecastX.addSize(this.size.W, this.size.H);

        var forecastY = new ObjectOfEnvirons("test", "test", ""); // Тестовый объект при смещении по Y
        forecastY.addCoordinates(this.coordinates.X, this.coordinates.Y+speed.Y*2);
        forecastY.addSize(this.size.W, this.size.H);
        
        var lastAcross = false;

        // Перебираем сектора
        for(var i in acrossSectors)
        {
            var sector = gameMap.getSector(acrossSectors[i].i, acrossSectors[i].j); // Текущий сектор

            // если прогнозируемый объект(смещенный по X) пересекатся не пересается с объектами пространства идет смещение по X
            if(!boundsCheckingEnvirons(forecastX)){
                var resAcross = pereborElementAcross(sector.elements,forecastX, this);
                if(resAcross === false){
                    this.addPosX(speed.X);  // Перемещение элемента по оси X
                }
                else
                {
                    lastAcross = resAcross;
                }
            }
            else{lastAcross = resAcross;}   
            
            // если прогнозируемый объект(смещенный по Y) пересекатся не пересается с объектами пространства идет смещение по Y
            if(!boundsCheckingEnvirons(forecastY)){
                resAcross = pereborElementAcross(sector.elements,forecastY, this)
                if(resAcross === false){
                    this.addPosY(speed.Y);  // Перемещение элемента по оси Y
                }
                else
                {
                    lastAcross = resAcross;
                }
            }
            else{lastAcross = resAcross;}
        }

        var afterStepAcrossSectors = gameMap.acrossSectors(this); // Получаем список секторов перекающихся с объектом
        if(afterStepAcrossSectors.length !== acrossSectors.length) gameMap.editElementInSector(acrossSectors, afterStepAcrossSectors, this);

        // Очистка памяти
        delete forecastX;
        delete forecastY;


//        // ------------- Отладочная информация --------------------------------------------------------------------------------------------------------------//
        this.visibleCoord(); // Отображение текущих координат объекта
//        // --------------------------------------------------------------------------------------------------------------------------------------------------//
        return lastAcross;
    };


    /**
     * http://ru.wikipedia.org/wiki/Полярная_система_координат
     * @param {type} vector
     * @returns {Wight.prototype.GetAngleTo@pro;status@pro;rotate|Number|Wight.prototype.GetAngleTo.a}
     */
    Wight.prototype.GetAngleTo = function( vector )
    {
        var v = {X:vector.X - this.coordinates.X, Y: vector.Y - this.coordinates.Y}
        v = normVector(v)
        
        var a = 0;
        if(v.X > 0)
        {
            a = Math.atan(v.Y/v.X)
        }
        else if(v.X < 0 &&  v.Y>= 0)
        {
            a = Math.atan(v.Y/v.X) + Math.PI
        }
        else if(v.X < 0 &&  v.Y < 0)
        {
            a = Math.atan(v.Y/v.X) - Math.PI
        }
        else if(v.X == 0 &&  v.Y > 0)
        {
            a = Math.PI/2
        }
        else if(v.X == 0 &&  v.Y < 0)
        {
            a = -Math.PI/2
        }
        else if(v.X == 0 &&  v.Y == 0)
        {
            a = 0
        }
       
       
        var ra =  a - this.status.rotate
        if( ra > Math.PI)
        {
            ra -= 2*Math.PI
            //console.warn("ra > (-) Math.PI:" + toDeg(ra) + "\t" + toDeg(  (ra - Math.PI - Math.PI )))
        }
        if( ra < -Math.PI)
        {
            ra += 2*Math.PI
            //console.warn("ra < (+) Math.PI:" + toDeg(ra) + "\t" + toDeg(  (ra - Math.PI - Math.PI )))
        }
        
        if(ra > 2*Math.PI)
        {
            ra -= 2*Math.PI
        }
         
        return ra;
    }
    
    

    /**
     * Разворот объекта за положением мышки
     * Разворот объекта rotate(jq_object_Environs, jq_object)
     * @param {jQueryObject} jq_object_Environs - объект пространства
     * @param {jQueryObject} jq_object - разворачиваемый объект
     * @global gameMap
     */
    Wight.prototype.setRotateToMouseCoord = function(jq_object_Environs, jq_object, coordDas){
        
        if(!this.life()){ // Проверка жив ли объект
            var Environs = gameMap.mainEnvirons; // Пространство

            //Координаты центра объекта
            var centerX = jq_object.position().left+(this.size.W/2)+Environs.marginLeft;
            var centerY = jq_object.position().top+(this.size.H/2)+Environs.marginTop;

            // Координаты мыши относительно страницы
            if(typeof(coordDas) == "undefined"){coordDas = mouseCoordinates();}

            //alert(mouseCoord+" "+mouseCoord);
            // Угол поворота к курсору
            var a = coordDas.X-centerX-jq_object_Environs.position().left;
            var c = Math.pow((Math.pow((coordDas.X-centerX-jq_object_Environs.position().left), 2) + Math.pow((coordDas.Y-centerY-jq_object_Environs.position().top), 2)), 1/2);
            var angleRotate = Math.acos(a/c)*180/Math.PI;

            if(coordDas.Y-jq_object_Environs.position().top < centerY){angleRotate = angleRotate*-1;}
            // Разворот объекта
            jq_object.rotate(angleRotate);
            // Запись сведений
        }
    };

    /**
     * Функция движения поля относительно сущности walkAroundTheEarth(element)
     * @param {Object} element - объект пространства
     * @global gameMap
     */
    Wight.prototype.walkAroundTheEarth = function(element){
        var environs = gameMap.getSizeEnvirons(); // Размеры пространства
        var environsWindow = gameMap.getSizeWindow(); // Размеры окна
        var mainEnvirons = gameMap.mainEnvirons;  // Данные окна
        // Проверка на расположение сущности в пространстве
        if(element.coordinates.X > environsWindow.W/2 && element.coordinates.X < environs.W-(environsWindow.W/2)){mainEnvirons.marginLeft = -element.coordinates.X+(environsWindow.W/2);}
        if(element.coordinates.Y > environsWindow.H/2 && element.coordinates.Y < environs.H-(environsWindow.H/2)){mainEnvirons.marginTop = -element.coordinates.Y+(environsWindow.H/2);}

        mainEnvirons.fill(); // Запись данных
    };

    Wight.prototype.life = function(){
        if(this.health <= 0){
            this.addStyle("backgroundImage", "url(img/dead_glaz.png)");
            if(!this.deleted){ // Проверка, удален ли элемент в DOM
                gameMap.addTrash(this.idObject);
                this.deleted = true;
            }
            return true;
        }
    };

    Wight.prototype.TimeTiсk = function( )
    {
        // Если объект жив то он движется
        if(!this.life()){
            stepObject(this);
        }       
        this.walkAroundTheEarth(this);
    };
    
    extend(Wight, ObjectOfEnvirons, "Wight -> ObjectOfEnvirons");
