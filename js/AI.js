//--------------------------------------------------------------------------------------------------------------------------//
//---------- Данная библиотека описывает сущностей с искуственным интелектом -----------------------------------------------//
//--------------------------------------------------------------------------------------------------------------------------//



function AI(name, id_object, class_object, owner){
    AI.superclass.constructor.call(this, name, id_object, class_object);
    extend(this, Wight, "this AI -> Wight"); // Наследование AI от Wight    
    
    this.editingHealth = this.health*1; // Сведения об изменении здоровья
    this.way;                           // Назначеный путь
    this.visibility = 150;              // Дальность обзора объекта
    this.vayBefore = 0;                 // Максимальный путь полученый объектом
    this.pointStartAI;                  // Точка начала пути объекта
    this.diapason = 120;                // угол обзора объекта
    this.mainPorpose;                   // Цель к которой стремится объект   
    this.owner = owner;                 // Указывает какому интелектуальному агенту принадлежит агент
    this.statusOfAction = {};                // Статус активности и возможные действия
    
    // Описание статусов
    this.statusOfAction["step"] = true;
    this.statusOfAction["attack"] = false;
    this.statusOfAction["wait"] = false;    
    
    this.fixedAim = [];
    
    
    addMinionForOwner(this.owner, this);  // При инициализации агента, он добавляется в массив владельца 
            
}

    /**
     * Функция поиска пути findWay(diapason, direction) поиск пути организуется по трем возможным тректориям в одной плоскости: траектория центра, крайнего левого положения и крайнего правого положения
     * @param {number} diapason     - Диапазон(задается в градусах)
     * @param {object} direction    - Направление
     */
    AI.prototype.findWay = function(direction){
        
        var visibleAim;     // Координаты видимой цели
        var freeWay = [];   // Пути в пространстве не пересекающиеся с объектами
        
        var beginAngle = 0; // Абсолютный начальный угол обзора
        var endAngle = 0;   // Центр обзора
        var angleMid = 0;   // Абсолютный конечный угол обзора
        
        if(typeof(direction) == "undefined"){endAngle = 360; beginAngle = 0;}
        else{
            // Получаем угол начала диапозона
            // Поиск в какой четверти находится угол
            if(this.center.X <= direction.X && this.center.Y <= direction.Y){angleMid = 90-Math.atan(Math.abs((direction.X-this.center.X))/Math.abs((direction.Y-this.center.Y)))*180/Math.PI; beginAngle = angleMid-(this.diapason/2); endAngle = angleMid+(this.diapason/2);}
            if(this.center.X > direction.X && this.center.Y <= direction.Y){angleMid = Math.atan(Math.abs((direction.X-this.center.X))/Math.abs((direction.Y-this.center.Y)))*180/Math.PI; beginAngle = (angleMid+90)-(this.diapason/2); endAngle = (angleMid+90)+(this.diapason/2); }
            if(this.center.X > direction.X && this.center.Y > direction.Y){angleMid = 90-Math.atan(Math.abs((direction.X-this.center.X))/Math.abs((direction.Y-this.center.Y)))*180/Math.PI; beginAngle = (angleMid+180)-(this.diapason/2); endAngle = (angleMid+180)+(this.diapason/2); }
            if(this.center.X <= direction.X && this.center.Y > direction.Y){angleMid = Math.atan(Math.abs((direction.X-this.center.X))/Math.abs((direction.Y-this.center.Y)))*180/Math.PI; beginAngle = (angleMid+270)-(this.diapason/2); endAngle = (angleMid+270)+(this.diapason/2); }
        }
        
        var oneOfTheWay = [];                           // Массив вероятных сторон
                
        // Осмотр на местности и поиск ближайшиж путей (видимость ограничена)
        for(var i = beginAngle; i < endAngle; i+=10){

            var outEnvirons = false; // Отметка о выходе траектории за пределы пространства (для корректировки маршрута)

            var usedAngle = i;
            if(usedAngle > 360){usedAngle-=360;}
            
            var coordRotate = angleRotate(this.center, {X: (this.center.X+this.visibility), Y:this.center.Y}, usedAngle);                   // Координата пути
            
            var coordLeftPosAI = angleRotate(this.center, {X: (this.center.X+(this.size.W/2)), Y:this.center.Y}, (usedAngle-90));         // Крайняя левая точка ИИ
            var coordRigthPosAI = angleRotate(this.center, {X: (this.center.X+(this.size.W/2)), Y:this.center.Y}, (usedAngle+90));        // Крайняя правая точка ИИ
            
            var coordLeftRotate = angleRotate(coordLeftPosAI, {X: (coordLeftPosAI.X+this.visibility), Y:coordLeftPosAI.Y}, usedAngle);      // Координата пути со смещением влево
            var coordRigthRotate = angleRotate(coordRigthPosAI, {X: (coordRigthPosAI.X+this.visibility), Y:coordRigthPosAI.Y}, usedAngle);  // Координата пути со смещением вправо            
            
                        
            // Фантомные объекты со смещением
            var fantomAILeft = new Spots(this.name, "fantomAILeft", this.classObject);                // Объект со смещением слева
            fantomAILeft.addCoordinates(coordLeftPosAI.X-(this.size.W/2), coordLeftPosAI.Y-(this.size.H/2));
            fantomAILeft.addSize(this.size.W, this.size.H);
            
            var fantomAIRigth = new Spots(this.name, "fantomAIRigth",this.classObject);             // Объект со смещением справа
            fantomAIRigth.addCoordinates(coordRigthPosAI.X-(this.size.W/2), coordRigthPosAI.Y-(this.size.H/2));
            fantomAIRigth.addSize(this.size.W, this.size.H);
            
            var spotC = new Spots("spotC", "spotC","");                // Точка конца пути центр
            spotC.addCoordinates(coordRotate.X, coordRotate.Y);
            spotC.addSize(1, 1);
            
            var spotL = new Spots("spotL", "spotL","");                // Точка конца пути слева
            spotL.addCoordinates(coordLeftRotate.X, coordLeftRotate.Y);
            spotL.addSize(1, 1);
            
            var spotR = new Spots("spotR", "spotR","");                // Точка конца пути справа
            spotR.addCoordinates(coordRigthRotate.X, coordRigthRotate.Y);
            spotR.addSize(1, 1);



            var x, y, w, h, k; // Вспомогательные переменные
            var traectories = {} // Массив траекторий
           
            traectories.wayC = new Spots("wayC","wayC",""); // Объект центральная траектория 
            x = 0; y = 0; w = 0; h = 0;
            if(coordRotate.X-this.center.X > 0){x = this.center.X; w = coordRotate.X-this.center.X;} 
            else if(coordRotate.X-this.center.X < 0){x = coordRotate.X; w = this.center.X-coordRotate.X;}
            else{x = coordRotate.X; w = 0;}
                    
            if(coordRotate.Y-this.center.Y > 0){y = this.center.Y; h = coordRotate.Y-this.center.Y;} 
            else if(coordRotate.Y-this.center.Y < 0){y = coordRotate.Y; h = this.center.Y-coordRotate.Y;}
            else{y = coordRotate.Y; h = 0;}
            
            traectories.wayC.addCoordinates(x,y);
            traectories.wayC.addSize(w,h);
            if(w == 0 || h == 0){traectories.kC = 1;}else{traectories.kC = w/h;}
            
            
            
            traectories.wayL = new Spots("wayL","wayL",""); // Объект левая траектория 
            x = 0; y = 0; w = 0; h = 0;
            if(coordLeftRotate.X-fantomAILeft.center.X > 0){x = fantomAILeft.center.X; w = coordLeftRotate.X-fantomAILeft.center.X;} 
            else if(coordLeftRotate.X-fantomAILeft.center.X < 0){x = coordLeftRotate.X; w = fantomAILeft.center.X-coordLeftRotate.X;}
            else{x = coordLeftRotate.X; w = 0;}
                    
            if(coordLeftRotate.Y-fantomAILeft.center.Y > 0){y = fantomAILeft.center.Y; h = coordLeftRotate.Y-fantomAILeft.center.Y;} 
            else if(coordLeftRotate.Y-fantomAILeft.center.Y < 0){y = coordLeftRotate.Y; h = fantomAILeft.center.Y-coordLeftRotate.Y;}
            else{y = coordLeftRotate.Y; h = 0;}
            
            traectories.wayL.addCoordinates(x,y);
            traectories.wayL.addSize(w,h);
            if(w == 0 || h == 0){traectories.kL = 1;}else{traectories.kL = w/h;}
            
            
            
            traectories.wayR = new Spots("wayR","wayR",""); // Объект правая траектория 
            x = 0; y = 0; w = 0; h = 0;
            if(coordRigthRotate.X-fantomAIRigth.center.X > 0){x = fantomAIRigth.center.X; w = coordRigthRotate.X-fantomAIRigth.center.X;} 
            else if(coordRigthRotate.X-fantomAIRigth.center.X < 0){x = coordRigthRotate.X; w = fantomAIRigth.center.X-coordRigthRotate.X;}
            else{x = coordRigthRotate.X; w = 0;}
                    
            if(coordRigthRotate.Y-fantomAIRigth.center.Y > 0){y = fantomAIRigth.center.Y; h = coordRigthRotate.Y-fantomAIRigth.center.Y;} 
            else if(coordRigthRotate.Y-fantomAIRigth.center.Y < 0){y = coordRigthRotate.Y; h = fantomAIRigth.center.Y-coordRigthRotate.Y;}
            else{y = coordRigthRotate.Y; h = 0;}
            
            traectories.wayR.addCoordinates(x,y);
            traectories.wayR.addSize(w,h);
            if(w == 0 || h == 0){traectories.kR = 1;}else{traectories.kR = w/h;}
                   
                                    
            // Корректировка координат траекторий при выходе их за пределы пространства
            if(boundsCheckingEnvirons(traectories.wayC)){                 
                outEnvirons = true;              
                correctionCoordinates(traectories.wayC, this, coordRotate, traectories.kC);
                correctionCoordinates(traectories.wayL, fantomAILeft, coordLeftRotate, traectories.kL);
                correctionCoordinates(traectories.wayR, fantomAIRigth, coordRigthRotate, traectories.kR);                                                                                                                     
            }

            /**
             * Функция поиска пересечения пули с объектами pereborElementsOfSector(traectory, sectors, shooter, spot)
             * @param {object} traectory    - Объект траектори
             * @param {object} shooter      - Сам ИИ
             * @param {object} spot         - Конечная точка траектории
             */
            function pereborElementsOfSector(traectory, shooter, spot){

                var sectors = gameMap.acrossSectors(traectory); // Получаем сектора через которые проходит траектория
                
                var controlElements = [];  // Хранит проверенные объекты
                var spotsArray = [];       // Хранит точки попадания в границы объетов
                var spotsWightArray = [];  // Хранит точки пересечения с сущностями

                // Перебор всех секторов
                for(var i in sectors){

                    var sector = gameMap.getSector(sectors[i].i, sectors[i].j); // Получение каждого отдельного сектора
                    
                    // Перебор элементов сектора
                    for(var j in sector.elements){
                        var element = sector.elements[j]; // Проверяемый элемент
                        if(element.name != shooter.name  && element.deleted != true)
                        if(!searchForMatches(controlElements, element)){     // Проверка на наличие повторного элемента

                            controlElements.push(element);                   // Если в массиве controlElements нет совпадающего элемента то он добавляется

                            if(element.classObject == "element" || element.classObject == shooter.classObject){            // Пока ИИ будет игнорировать стрелков
                            
                                if(element.acrossObject(traectory)){         // Наличие пересечения траектории с объектом

                                    var newSpot = new Spots("newSpot", "newSpot","");   // Объект пересечения траектории с границей объекта
                                    newSpot.addSize(1, 1);  

                                    edLineAcross(spot.coordinates,shooter.center,{X:element.coordinates.X, Y:0}, spotsArray, element, newSpot, traectory);                            // Проверка пересечения с левой стороной
                                    edLineAcross(spot.coordinates,shooter.center,{X:element.coordinates.X+element.size.W, Y:0}, spotsArray, element, newSpot, traectory);  // Проверка пересечения с правой стороной
                                    edLineAcross(spot.coordinates,shooter.center,{X:0, Y:element.coordinates.Y}, spotsArray, element, newSpot, traectory);                            // Проверка пересечения с нижней стороной
                                    edLineAcross(spot.coordinates,shooter.center,{X:0, Y:element.coordinates.Y+element.size.H}, spotsArray, element, newSpot, traectory);  // Проверка пересечения с верхней стороной                               
                                    delete newSpot;                                                                       
                                }
                            }
                            // Если пуля попадает в другого стрелка у него онимается жизнь
                            else if(element.classObject != "element" && element.classObject != shooter.classObject && element != shooter){
                                if(!boundsCheckingEnvirons(traectory)){ // Проверка превосходит ли траектория пространство
                                    var newSpot = new Spots("newSpot", "newSpot","");   // Объект пересечения траектории с границей объекта
                                    newSpot.addSize(1, 1);                                         
                                    edLineAcross(spot.coordinates,shooter.center,{X:element.coordinates.X, Y:0}, spotsWightArray, element, newSpot, traectory);                            // Проверка пересечения с левой стороной
                                    edLineAcross(spot.coordinates,shooter.center,{X:element.coordinates.X+element.size.W, Y:0}, spotsWightArray, element, newSpot, traectory);  // Проверка пересечения с правой стороной
                                    edLineAcross(spot.coordinates,shooter.center,{X:0, Y:element.coordinates.Y}, spotsWightArray, element, newSpot, traectory);                            // Проверка пересечения с нижней стороной
                                    edLineAcross(spot.coordinates,shooter.center,{X:0, Y:element.coordinates.Y+element.size.H}, spotsWightArray, element, newSpot, traectory);  // Проверка пересечения с верхней стороной                               
                                    delete newSpot; // Очистка памяти
                                }                                
                            }
                        }
                    }
                }
                
                // Если есть хоть одна точка пересечений
                if(spotsArray.length > 0){
                    
                    var mixlength = 100000000;              // Условное максимальное значение
                    var mixlengthShoot = spotsArray[0];     // Условное ближайшее пересечение с границей

                    // Перебираем все пересечения с границами внутри объекта
                    for(var i in spotsArray){
                        
                        var lengthShoot = Math.pow(Math.pow((spotsArray[i].X-shooter.coordinates.X),2)+Math.pow((spotsArray[i].Y-shooter.coordinates.Y),2),0.5); // длина траектории до пересекаемого объекта
                        if(lengthShoot < mixlength){ 
                            mixlength = lengthShoot;
                            mixlengthShoot = spotsArray[i];
                        }
                    }
                    return mixlengthShoot; // Возвращаем ближайшее значение
                }
                
                // Если не точек но есть пересечение с враждебной сущностью
                if(spotsWightArray.length > 0){
                    
                    var mixlengthAim = 100000000;                   // Условное максимальное значение
                    var mixlengthShootAim = spotsWightArray[0];     // Условное ближайшее пересечение с границей
                    
                    // Перебираем все пересечения с границами внутри объекта
                    for(var i in spotsWightArray){
                        
                        var lengthShoot = Math.pow(Math.pow((spotsWightArray[i].X-shooter.coordinates.X),2)+Math.pow((spotsWightArray[i].Y-shooter.coordinates.Y),2),0.5); // длина траектории до пересекаемого объекта
                        if(lengthShoot < mixlengthAim){ 
                            mixlengthAim = lengthShoot;
                            mixlengthShootAim = spotsWightArray[i].link;        // Запись ближайшего враждебного объекта
                            visibleAim = mixlengthShootAim;                     // Запись ближайшего враждебного объекта как видимого
                            
                            if(spotsWightArray[i].link.health > 0 && shooter.owner != undefined){             // Если враждебный объект жив, то он запоминается ИИ как цель
                                globaInteligens[shooter.owner].setAim({aim: mixlengthShootAim, coordinates: mixlengthShootAim.center}); // Агетном замечена цель и сведения отправлены владельцу агента
                                shooter.addFixedAim(mixlengthShootAim); // Заполнение массива зафиксированных целей
                                shooter.activationStatus("wait");
                            }
                        }
                    }               
                }                
            }
            var arrayAllWay = [];   // Хранилище путей с фантомными траекториями
            var arrayFreeWay = [];  // Хранилище путей с фантомными траекториями не пересекающихся с объектами
                       
            var acrossSpot = pereborElementsOfSector(traectories.wayC, this, spotC); // Поиск столкновения траектории с объектами и пространством
            
            // Добавление вероятного пути
            if(typeof(acrossSpot) != 'object'){ // Если не найдено ни одного столкновения траектории с объектами и пространством
                var lengthEndWay = getGipotenuza(Math.abs(this.center.X-coordRotate.X), Math.abs(this.center.Y-coordRotate.Y));
                arrayAllWay.push({length:lengthEndWay, X:coordRotate.X, Y:coordRotate.Y, probability:0, stolk:false});
                if(!outEnvirons){arrayFreeWay.push({length:lengthEndWay, X:coordRotate.X, Y:coordRotate.Y, probability:0, stolk:false});} // Фиксирование свободных путей
            }          
            else{ // Если найдено столкновение траектории с объектами и пространством
                var lengthEndWay = getGipotenuza(Math.abs(this.center.X-acrossSpot.X), Math.abs(this.center.Y-acrossSpot.Y));
                arrayAllWay.push({length:lengthEndWay, X:acrossSpot.X, Y:acrossSpot.Y, probability:0, stolk:true});              
            }
            acrossSpot = undefined;
            
            acrossSpot = pereborElementsOfSector(traectories.wayL, fantomAILeft, spotL); // Поиск столкновения траектории с объектами и пространством
            // Добавление вероятного пути
            if(typeof(acrossSpot) != 'object'){ // Если не найдено ни одного столкновения траектории с объектами и пространством
                var lengthEndWay = getGipotenuza(Math.abs(this.center.X-coordLeftRotate.X), Math.abs(this.center.Y-coordLeftRotate.Y));
                arrayAllWay.push({length:lengthEndWay, X:coordLeftRotate.X, Y:coordLeftRotate.Y, probability:0, stolk:false});
                if(!outEnvirons){arrayFreeWay.push({length:lengthEndWay, X:coordLeftRotate.X, Y:coordLeftRotate.Y, probability:0, stolk:false});} // Фиксирование свободных путей
            }          
            else{ // Если найдено столкновение траектории с объектами и пространством
                var lengthEndWay = getGipotenuza(Math.abs(this.center.X-acrossSpot.X), Math.abs(this.center.Y-acrossSpot.Y));
                arrayAllWay.push({length:lengthEndWay, X:acrossSpot.X, Y:acrossSpot.Y, probability:0, stolk:true});              
            }
            acrossSpot = undefined;
            
            acrossSpot = pereborElementsOfSector(traectories.wayR, fantomAIRigth, spotR); // Поиск столкновения траектории с объектами и пространством
            // Добавление вероятного пути
            if(typeof(acrossSpot) != 'object'){ // Если не найдено ни одного столкновения траектории с объектами и пространством
                var lengthEndWay = getGipotenuza(Math.abs(this.center.X-coordRigthRotate.X), Math.abs(this.center.Y-coordRigthRotate.Y));
                arrayAllWay.push({length:lengthEndWay, X:coordRigthRotate.X, Y:coordRigthRotate.Y, probability:0, stolk:false});
                if(!outEnvirons){arrayFreeWay.push({length:lengthEndWay, X:coordRigthRotate.X, Y:coordRigthRotate.Y, probability:0, stolk:false});} // Фиксирование свободных путей
            }          
            else{ // Если найдено столкновение траектории с объектами и пространством
                var lengthEndWay = getGipotenuza(Math.abs(this.center.X-acrossSpot.X), Math.abs(this.center.Y-acrossSpot.Y));
                arrayAllWay.push({length:lengthEndWay, X:acrossSpot.X, Y:acrossSpot.Y, probability:0, stolk:true});              
            }                      
            
            if(arrayAllWay[0].stolk == false && arrayAllWay[1].stolk == false && arrayAllWay[2].stolk == false){oneOfTheWay.push(arrayAllWay[0]); if(arrayFreeWay.length != 0){freeWay.push(arrayFreeWay[0]);}} // Когда все точки не попадают на объекты 
            if(arrayAllWay[0].stolk == false && arrayAllWay[1].stolk == true && arrayAllWay[2].stolk == false){oneOfTheWay.push(arrayAllWay[2]); }  // Когда левая точка попадает на объект 
            if(arrayAllWay[0].stolk == false && arrayAllWay[1].stolk == false && arrayAllWay[2].stolk == true){oneOfTheWay.push(arrayAllWay[1]); }  // Когда правая точка попадает на объект
            if(arrayAllWay[1].stolk == true && arrayAllWay[2].stolk == true){oneOfTheWay.push(arrayAllWay[0]);} // Если центральная точка пересекается с объектом то выбирается максимальный из вероятных путей                        
        }
        
        // Проверка на то заметил ли враждеюный агент, если заметил то переход в режим атаки
        for(var i in this.fixedAim){ // Перебор зафиксированных объектов
            for(var j in this.fixedAim[i].fixedAim){ // Перебор того что увидел зафиксированный объект
                if(this.fixedAim[i].fixedAim[j] == this){ 
                    this.activationStatus("attack");
                }
            }
        }
        
        if(visibleAim == undefined){
            this.fixedAim = [];
            this.activationStatus("step");
        }
        
        
        // Установка вероятности возможным путям           
        var sumWay = 0; // Сумма всех длин путей
        var maximumWidth = 0;
        var maxWiId = 0; // ID пути с максимальной длинной
        var minimumFreeWay = {width:Math.pow(10,10), X:0, Y:0}; // Минимальная длина пути к цели
        
        for(var i in oneOfTheWay){
            if(oneOfTheWay[i].length > maximumWidth){maximumWidth = oneOfTheWay[i].length; maxWiId = i;}
            sumWay += oneOfTheWay[i].length;
        }
            
        var sumProb = 0; // Сумма всех вероятностей
        for(var i in oneOfTheWay){
            sumProb += oneOfTheWay[i].length/(sumWay/100);
            oneOfTheWay[i].probability = sumProb;               
        }
        
        
        // Проверка на тупик, если предыдущий путь длиннее чем текущий то диапазон повора увеличивается
        if(this.vayBefore < maximumWidth){this.vayBefore = maximumWidth; this.diapason = 120;}
        else{this.diapason = 180;}
        
        
        
        if(this.mainPorpose == undefined){ // Если цель отсутвует то стандартный выбор пути
            var probability = getRandomInt(0,100); // Вероятность
            if(probability < 95){
                this.way = {X:oneOfTheWay[maxWiId].X, Y:oneOfTheWay[maxWiId].Y, A:visibleAim};
                return {X:oneOfTheWay[maxWiId].X, Y:oneOfTheWay[maxWiId].Y, A:visibleAim};
            }; // Дорога судьбы 
            
            probability = getRandomInt(0,100); // Дополнительная вероятность
            for(var i in freeWay){
                if(freeWay[i].probability > probability){
                    this.way = {X:freeWay[i].X, Y:freeWay[i].Y, A:visibleAim};
                    return {X:freeWay[i].X, Y:freeWay[i].Y, A:visibleAim};
                }; // Дорога судьбы              
            }
        }              
        else{ // Выбор наилучшего пути к цели
            if(freeWay.length == 0){
                this.findWay();
            }
            for(var i in freeWay){
                
                var AI_WAY = getGipotenuza((this.center.X-freeWay[i].X), (this.center.Y-freeWay[i].Y));                 // Растояние между ИИ и везможным путем
                var WAY_PORPOSE = getGipotenuza((this.mainPorpose.X-freeWay[i].X), (this.mainPorpose.Y-freeWay[i].Y));  // Растояние между везможным путем и целью
                // Поиск наименьшего пути
                if(minimumFreeWay.width > (AI_WAY+WAY_PORPOSE)){
                    minimumFreeWay.width = AI_WAY+WAY_PORPOSE; 
                    minimumFreeWay.X = freeWay[i].X; 
                    minimumFreeWay.Y = freeWay[i].Y;
                }
            }
            this.way = {X:minimumFreeWay.X, Y:minimumFreeWay.Y, A:visibleAim};
            return {X:minimumFreeWay.X, Y:minimumFreeWay.Y, A:visibleAim};
        }
    };
    
    
    /**
     * Функция поиска кратчайшего пути от текущего местоположения до целевой точки findShortWay(direction)
     * @param {object} direction    - Направление
     * @return {number} shortWay - длину кратчайшего пути до точки
     */
    AI.prototype.findShortWay = function(direction){
              
        var beginAngle = 0; // Абсолютный начальный угол обзора
        var endAngle = 0;   // Центр обзора
        var angleMid = 0;   // Абсолютный конечный угол обзора
        
        var diap = 180;
        
        var bWay = this.center; // Начало пути
        var preWay = this.center; // Точка коррекции пути
        var eWay = direction; // Конец пути
        var visibility = 30;
        var allWay = 0; // Общий путь
        
        do{
            var freeWay = [];   // Пути в пространстве не пересекающиеся с объектами
            if(preWay == this.center){endAngle = 360; beginAngle = 0;}
            else{
                // Получаем угол начала диапозона
                // Поиск в какой четверти находится угол
                if(bWay.X <= preWay.X && bWay.Y <= preWay.Y){angleMid = 90-Math.atan(Math.abs((preWay.X-bWay.X))/Math.abs((preWay.Y-bWay.Y)))*180/Math.PI; beginAngle = angleMid-(diap/2); endAngle = angleMid+(diap/2);}
                if(bWay.X > preWay.X && bWay.Y <= preWay.Y){angleMid = Math.atan(Math.abs((preWay.X-bWay.X))/Math.abs((preWay.Y-bWay.Y)))*180/Math.PI; beginAngle = (angleMid+90)-(diap/2); endAngle = (angleMid+90)+(diap/2); }
                if(bWay.X > preWay.X && bWay.Y > preWay.Y){angleMid = 90-Math.atan(Math.abs((preWay.X-bWay.X))/Math.abs((preWay.Y-bWay.Y)))*180/Math.PI; beginAngle = (angleMid+180)-(diap/2); endAngle = (angleMid+180)+(diap/2); }
                if(bWay.X <= preWay.X && bWay.Y > preWay.Y){angleMid = Math.atan(Math.abs((preWay.X-bWay.X))/Math.abs((preWay.Y-bWay.Y)))*180/Math.PI; beginAngle = (angleMid+270)-(diap/2); endAngle = (angleMid+270)+(diap/2); }
                if(diap == 270){diap = 180;}
                bWay = preWay;
            }
            
            
            var oneOfTheWay = [];                           // Массив вероятных сторон

            // Осмотр на местности и поиск ближайшиж путей (видимость ограничена)
            for(var i = beginAngle; i < endAngle; i+=10){

                var outEnvirons = false; // Отметка о выходе траектории за пределы пространства (для корректировки маршрута)

                var usedAngle = i;
                if(usedAngle > 360){usedAngle-=360;}

                var coordRotate = angleRotate(bWay, {X: (bWay.X+visibility), Y:bWay.Y}, usedAngle);                   // Координата пути

                // Фантомные объекты со смещением
                var fantomAI = new Spots(this.name, "fantomAILeft", this.classObject);                // Объект со смещением слева
                fantomAI.addCoordinates(bWay.X-(5), bWay.Y-(5));
                fantomAI.addSize(10, 10);

                var spotC = new Spots("spotC", "spotC","");                // Точка конца пути центр
                spotC.addCoordinates(coordRotate.X, coordRotate.Y);
                spotC.addSize(1, 1);

                var x, y, w, h, k; // Вспомогательные переменные

                var wayC = new Spots("wayC","wayC",""); // Объект центральная траектория 
                x = 0; y = 0; w = 0; h = 0;
                if(coordRotate.X-bWay.X > 0){x = bWay.X; w = coordRotate.X-bWay.X;} 
                else if(coordRotate.X-bWay.X < 0){x = coordRotate.X; w = bWay.X-coordRotate.X;}
                else{x = coordRotate.X; w = 0;}

                if(coordRotate.Y-bWay.Y > 0){y = bWay.Y; h = coordRotate.Y-bWay.Y;} 
                else if(coordRotate.Y-bWay.Y < 0){y = coordRotate.Y; h = bWay.Y-coordRotate.Y;}
                else{y = coordRotate.Y; h = 0;}

                wayC.addCoordinates(x,y);
                wayC.addSize(w,h);
                if(w == 0 || h == 0){wayC.kC = 1;}else{wayC.kC = w/h;}


                // Корректировка координат траекторий при выходе их за пределы пространства
                if(boundsCheckingEnvirons(wayC)){                 
                    outEnvirons = true;              
                    correctionCoordinates(wayC, fantomAI, coordRotate, wayC.kC);                                                                                                                
                }

                /**
                 * Функция поиска пересечения пули с объектами pereborElementsOfSector(traectory, sectors, shooter, spot)
                 * @param {object} traectory    - Объект траектори
                 * @param {object} shooter      - Сам ИИ
                 * @param {object} spot         - Конечная точка траектории
                 */
                function pereborElementsOfSector(traectory, shooter, spot){

                    var sectors = gameMap.acrossSectors(traectory); // Получаем сектора через которые проходит траектория

                    var controlElements = [];  // Хранит проверенные объекты
                    var spotsArray = [];       // Хранит точки попадания в границы объетов

                    // Перебор всех секторов
                    for(var i in sectors){
                        var sector = gameMap.getSector(sectors[i].i, sectors[i].j); // Получение каждого отдельного сектора                   
                        // Перебор элементов сектора
                        for(var j in sector.elements){
                            var element = sector.elements[j]; // Проверяемый элемент
                            if(element.name != shooter.name  && element.deleted != true)
                            if(!searchForMatches(controlElements, element)){     // Проверка на наличие повторного элемента

                                controlElements.push(element);                   // Если в массиве controlElements нет совпадающего элемента то он добавляется

                                if(element.classObject == "element" || element.classObject == shooter.classObject){            // Пока ИИ будет игнорировать стрелков

                                    if(element.acrossObject(traectory)){         // Наличие пересечения траектории с объектом

                                        var newSpot = new Spots("newSpot", "newSpot","");   // Объект пересечения траектории с границей объекта
                                        newSpot.addSize(1, 1);  

                                        edLineAcross(spot.coordinates,shooter.center,{X:element.coordinates.X, Y:0}, spotsArray, element, newSpot, traectory);                            // Проверка пересечения с левой стороной
                                        edLineAcross(spot.coordinates,shooter.center,{X:element.coordinates.X+element.size.W, Y:0}, spotsArray, element, newSpot, traectory);  // Проверка пересечения с правой стороной
                                        edLineAcross(spot.coordinates,shooter.center,{X:0, Y:element.coordinates.Y}, spotsArray, element, newSpot, traectory);                            // Проверка пересечения с нижней стороной
                                        edLineAcross(spot.coordinates,shooter.center,{X:0, Y:element.coordinates.Y+element.size.H}, spotsArray, element, newSpot, traectory);  // Проверка пересечения с верхней стороной                               
                                        delete newSpot;                                                                       
                                    }
                                }
                            }
                        }
                    }

                    // Если есть хоть одна точка пересечений
                    if(spotsArray.length > 0){

                        var mixlength = 100000000;              // Условное максимальное значение
                        var mixlengthShoot = spotsArray[0];     // Условное ближайшее пересечение с границей

                        // Перебираем все пересечения с границами внутри объекта
                        for(var i in spotsArray){

                            var lengthShoot = Math.pow(Math.pow((spotsArray[i].X-shooter.coordinates.X),2)+Math.pow((spotsArray[i].Y-shooter.coordinates.Y),2),0.5); // длина траектории до пересекаемого объекта
                            if(lengthShoot < mixlength){ 
                                mixlength = lengthShoot;
                                mixlengthShoot = spotsArray[i];
                            }
                        }
                        return mixlengthShoot; // Возвращаем ближайшее значение
                    }                                            
                }

                var acrossSpot = pereborElementsOfSector(wayC, fantomAI, spotC); // Поиск столкновения траектории с объектами и пространством

                // Добавление вероятного пути
                if(typeof(acrossSpot) != 'object'){ // Если не найдено ни одного столкновения траектории с объектами и пространством
                    var lengthEndWay = getGipotenuza(Math.abs(bWay.X-coordRotate.X), Math.abs(bWay.Y-coordRotate.Y));
                    oneOfTheWay.push({length:lengthEndWay, X:coordRotate.X, Y:coordRotate.Y, probability:0});
                    if(!outEnvirons){freeWay.push({length:lengthEndWay, X:coordRotate.X, Y:coordRotate.Y, probability:0});} // Фиксирование свободных путей
                }          
                else{ // Если найдено столкновение траектории с объектами и пространством
                    var lengthEndWay = getGipotenuza(Math.abs(bWay.X-acrossSpot.X), Math.abs(bWay.Y-acrossSpot.Y));
                    oneOfTheWay.push({length:lengthEndWay, X:acrossSpot.X, Y:acrossSpot.Y, probability:0});              
                }                                        
            }
   
            var minimumFreeWay = {width:Math.pow(10,10), X:0, Y:0}; // Минимальная длина пути к цели


            // Проверка на тупик, если предыдущий путь длиннее чем текущий то диапазон повора увеличивается
//            if(this.vayBefore < maximumWidth){this.vayBefore = maximumWidth; diap = 120;}
//            else{diap = 180;}

            if(freeWay.length == 0){
                diap = 270;
            }
            else{
                for(var i in freeWay){

                    var AI_WAY = getGipotenuza((bWay.X-freeWay[i].X), (bWay.Y-freeWay[i].Y));                 // Растояние между ИИ и везможным путем
                    var WAY_PORPOSE = getGipotenuza((direction.X-freeWay[i].X), (direction.Y-freeWay[i].Y));  // Растояние между везможным путем и целью
                    var AI_PORPOSE = getGipotenuza((bWay.X-direction.X), (bWay.Y-direction.Y));
                    // Поиск наименьшего пути
                    if(minimumFreeWay.width > (AI_WAY+WAY_PORPOSE)){
                        minimumFreeWay.width = AI_WAY+WAY_PORPOSE;
                        minimumFreeWay.AI_WAY = AI_WAY;
                        minimumFreeWay.X = freeWay[i].X; 
                        minimumFreeWay.Y = freeWay[i].Y;
                        if(AI_WAY > WAY_PORPOSE){
                            minimumFreeWay.AI_WAY = AI_PORPOSE;
                            minimumFreeWay.X = direction.X; 
                            minimumFreeWay.Y = direction.Y;
                            break;
                        }
                    }
                }

                preWay = {X:minimumFreeWay.X, Y:minimumFreeWay.Y};
                allWay+=minimumFreeWay.AI_WAY;
            }
        }
        while(bWay.X != eWay.X && bWay.Y != eWay.Y);
        return allWay;
    };
    
    /**
     * Функция мониторинга ИИ на изменение здоровья monitoringHealth()
     */
    AI.prototype.addFixedAim = function(fixedAim){
        var consilience = false; 
        for(var i in this.fixedAim){
            if(this.fixedAim[i] == fixedAim){
                consilience = true;
            }
        }
        if(!consilience){
            this.fixedAim.push(fixedAim);
        }
    };

    /**
     * Функция мониторинга ИИ на изменение здоровья monitoringHealth()
     */
    AI.prototype.monitoringHealth = function(){
        if(this.editingHealth != this.health){
            this.editingHealth = this.health;
            this.findWay();
        }
    };
        
    /**
     * Функция коррекции координат. Обрезает координаты если они выходят за пределы пространства correctionCoordinates(way, begin, coordRotate, k)
     * @param {object} way      - Траектория
     * @param {object} begin    - Начало координат
     * @param {object} end      - Конец координат
     * @param {object} k        - Коофициент отношения высоты к ширине
     */
    function correctionCoordinates(way, begin, end, k){
        var sizeEnvirons = gameMap.getSizeEnvirons();                   
        
        // По горизонтали
        if(way.coordinates.X < 0){
                    
            way.size.W = way.size.W+way.coordinates.X; 
            way.coordinates.X = 0;
                    
            if(begin.center.Y > end.Y){ end.Y = end.Y-end.X/k;}
            else if(begin.center.Y < end.Y){ end.Y = end.Y+end.X/k;}
            end.X = 0;
        }
        if(way.coordinates.X+way.size.W > sizeEnvirons.W){
                    
            way.size.W = sizeEnvirons.W-way.coordinates.X;
                    
            if(begin.center.Y > end.Y){ end.Y = end.Y-(sizeEnvirons.W-end.X)/k;}
            else if(begin.center.Y < end.Y){ end.Y = end.Y+(sizeEnvirons.W-end.X)/k;}                    
            end.Y = end.Y+(sizeEnvirons.W-end.X)/k;
            end.X = sizeEnvirons.W;
        }
                
        // По вертикали
        if(way.coordinates.Y < 0){
                    
            way.size.H = way.size.H+way.coordinates.Y; 
            way.coordinates.Y = 0;
                    
            if(begin.center.X > end.X){ end.X = end.X-end.Y*k;}
            else if(begin.center.X < end.X){ end.X = end.X+end.Y*k;}                 
            end.Y = 0;
        }
        if(way.coordinates.Y+way.size.H > sizeEnvirons.H){
                    
            way.size.H = sizeEnvirons.H-way.coordinates.Y;
                    
            if(begin.center.X > end.X){ end.X = end.X-(sizeEnvirons.H-end.Y)*k;}
            else if(begin.center.X < end.X){ end.X = end.X+(sizeEnvirons.H-end.Y)*k;}         
            end.Y = sizeEnvirons.H;
        }                         
    }
    
    
    
    
    /**
     * Функция движения агента stepAI()
     */
    function stepAI(Agent){

        var shoot = false;
            
        // По достижении ИИ главной цели
        if(Agent.mainPorpose != undefined){
            var mainPorpose = new Spots("mainPorpose", "mainPorpose", "mainPorpose");
            mainPorpose.addCoordinates(Agent.mainPorpose.X, Agent.mainPorpose.Y);
            mainPorpose.addSize(1,1);
            if(Agent.acrossObject(mainPorpose)){
                Agent.mainPorpose = undefined;
                Agent.findWay();
            }
            delete mainPorpose;
        }
                           
        // Проверка на затуп
        if(Agent.way == undefined){
            Agent.findWay();
        }
        // ИИ движется пока пока не дойдет до конца пути 
            
        else if(Agent.center.X != Agent.way.X || Agent.center.Y != Agent.way.Y){     
                
            Agent.setRotateToMouseCoord($("#main"), $("#"+Agent.name), Agent.way);
                
            var w = Math.abs(Agent.center.X - Agent.way.X);
            var h = Math.abs(Agent.center.Y - Agent.way.Y);
            var g = getGipotenuza(w,h);

            var step = processingStep(Agent.center, Agent.way, 2); // Получаем координаты смещения            
                
            // ищим путь каждые 10% от пройденого пути
            var w10 = Math.abs(Agent.pointStartAI.X - Agent.way.X);
            var h10 = Math.abs(Agent.pointStartAI.Y - Agent.way.Y);
            var g10 = getGipotenuza(w10,h10);
            var dk = g/(g10/100);
            if(dk < 80){                  
                Agent.findWay({X:Agent.way.X+(step.X), Y:Agent.way.Y+(step.Y)});                
                Agent.pointStartAI = {X:Agent.center.X, Y:Agent.center.Y};
            }
                
                
            // Проверка на будующие пересечения              
                
            var countAcrossOs = 0;
                
            for(var k = 0; k < 2; k++){
                    
                var controlElements = [];  // Хранит проверенные объекты
                // Создаем елемент проверки на возможные столкновения с элементами
                var porgnozAI = new Spots(Agent.name, Agent.name, "striker");
                if(k == 0){porgnozAI.addCoordinates((Agent.coordinates.X+2*step.X), (Agent.coordinates.Y));}
                if(k == 1){porgnozAI.addCoordinates((Agent.coordinates.X), (Agent.coordinates.Y+2*step.Y));}
                porgnozAI.addSize(32, 32);
                var acrossSectors = gameMap.acrossSectors(porgnozAI);
                for(var i in acrossSectors){
                    var sector = gameMap.getSector(acrossSectors[i].i, acrossSectors[i].j); // Получение каждого отдельного сектора

                    for(var j in sector.elements){
                        if(sector.elements[j].name != porgnozAI.name && sector.elements[j].deleted != true)
                        if(!searchForMatches(controlElements, sector.elements[j])){ // Проверка на наличие повторного элемента

                            controlElements.push(sector.elements[j]); // Если в массиве controlElements нет совпадающего элемента то он добавляется
                            if(sector.elements[j].acrossObject(porgnozAI)){
                                // При наличии будующего пересечения                                      
                                if(k == 0){step.X = 0; countAcrossOs++;} // Обнуляем текущий шаг
                                if(k == 1){step.Y = 0; countAcrossOs++;} // Обнуляем текущий шаг
                                if(countAcrossOs == 2){
                                    Agent.findWay();                                // Создаем новый путь
                                    Agent.pointStartAI = {X:Agent.center.X, Y:Agent.center.Y};  // Переназначаем точку старта}
                                }                                                                                                        
                                break;
                            }
                        }
                    }
                }
                delete porgnozAI;
            }                
            Agent.step(step.X, step.Y);                                       
        }
        else{Agent.findWay();}
    }
    
    /**
     * Функция атаки агента stepWithAttacAI()
     */
    function stepWithAttacAI(Agent){

        var shoot = false;
                                      
        // Проверка на затуп
        if(Agent.way == undefined){
            Agent.findWay();
        }
        // ИИ движется пока пока не дойдет до конца пути 
            
        else if(Agent.center.X != Agent.way.X || Agent.center.Y != Agent.way.Y){     
                
            if(Agent.way.A != undefined && Agent.way.A.health != 0){
                Agent.setRotateToMouseCoord($("#main"), $("#"+Agent.name), Agent.way.A.center);
                Agent.Armor.shoot(Agent, Agent.way.A.center);
            }
                
            var w = Math.abs(Agent.center.X - Agent.way.X);
            var h = Math.abs(Agent.center.Y - Agent.way.Y);
            var g = getGipotenuza(w,h);

            var step = processingStep(Agent.center, Agent.way, 2); // Получаем координаты смещения            
                
            // ищим путь каждые 10% от пройденого пути
            var w10 = Math.abs(Agent.pointStartAI.X - Agent.way.X);
            var h10 = Math.abs(Agent.pointStartAI.Y - Agent.way.Y);
            var g10 = getGipotenuza(w10,h10);
            var dk = g/(g10/100);
            if(dk < 80){                  
                Agent.findWay({X:Agent.way.X+(step.X), Y:Agent.way.Y+(step.Y)});                
                Agent.pointStartAI = {X:Agent.center.X, Y:Agent.center.Y};
            }               
                
            // Проверка на будующие пересечения              
                
            var countAcrossOs = 0;
                
            for(var k = 0; k < 2; k++){
                    
                var controlElements = [];  // Хранит проверенные объекты
                // Создаем елемент проверки на возможные столкновения с элементами
                var porgnozAI = new Spots(Agent.name, Agent.name, "striker");
                if(k == 0){porgnozAI.addCoordinates((Agent.coordinates.X+2*step.X), (Agent.coordinates.Y));}
                if(k == 1){porgnozAI.addCoordinates((Agent.coordinates.X), (Agent.coordinates.Y+2*step.Y));}
                porgnozAI.addSize(32, 32);
                var acrossSectors = gameMap.acrossSectors(porgnozAI);
                for(var i in acrossSectors){
                    var sector = gameMap.getSector(acrossSectors[i].i, acrossSectors[i].j); // Получение каждого отдельного сектора

                    for(var j in sector.elements){
                        if(sector.elements[j].name != porgnozAI.name && sector.elements[j].deleted != true)
                        if(!searchForMatches(controlElements, sector.elements[j])){ // Проверка на наличие повторного элемента

                            controlElements.push(sector.elements[j]); // Если в массиве controlElements нет совпадающего элемента то он добавляется
                            if(sector.elements[j].acrossObject(porgnozAI)){
                                // При наличии будующего пересечения                                      
                                if(k == 0){step.X = 0; countAcrossOs++;} // Обнуляем текущий шаг
                                if(k == 1){step.Y = 0; countAcrossOs++;} // Обнуляем текущий шаг
                                if(countAcrossOs == 2){
                                    Agent.findWay();                                // Создаем новый путь
                                    Agent.pointStartAI = {X:Agent.center.X, Y:Agent.center.Y};  // Переназначаем точку старта}
                                }                                                                                                        
                                break;
                            }
                        }
                    }
                }
                delete porgnozAI;
            }                
            Agent.step(step.X, step.Y);                                       
        }
        else{Agent.findWay();}
        // Проверка объекта на жизнедеятельность
        var lifeEnemy = false; 
        for(var i in Agent.fixedAim){
            if(!Agent.fixedAim[i].life()){lifeEnemy = true;}
        }
        if(!lifeEnemy){ // Когда все замеченные объекты мертвы агент вновь переходит в стадию хотьбы
            Agent.fixedAim = [];
            Agent.activationStatus("step");
            Agent.mainPorpose = undefined;
        }
    }
    
    /**
     * Функция атаки агента stepWithAttacAI()
     */
    function waitAI(Agent){
                                    
        // Проверка на затуп
        if(Agent.way == undefined){
            Agent.findWay();
        }               
        if(Agent.fixedAim[0] != undefined){
            Agent.setRotateToMouseCoord($("#main"), $("#"+Agent.name), Agent.fixedAim[0].center);
        }                                                                                      
    }
    
    /**
     * Функция тайминга TimeTiсk()
     */
    AI.prototype.activationStatus = function(name)
    {        
        // Выключаем все статусы
        for(var i in this.statusOfAction){
            this.statusOfAction[i] = false;
        }
        this.statusOfAction[name] = true; // Назначаем активность необходимому
        
    };
    
    /**
     * Функция тайминга TimeTiсk()
     */
    AI.prototype.TimeTiсk = function()
    {        
        // Агент действует по жизнеспособен
        if(!this.life()){ 
            if(this.statusOfAction["step"] == true){
                stepAI(this);
            };
            if(this.statusOfAction["attack"] == true){
                stepWithAttacAI(this);
            };
            if(this.statusOfAction["wait"] == true){
                waitAI(this);
            };
        }

        this.monitoringHealth();
        
    };
    extend(AI, Wight);
    
/**
 * Функция присвоения агента владельцу addMinionForOwner(owner, minion)
 * @param {object} owner      - Владелец агента
 * @param {object} minion     - Сам агент
 */
function addMinionForOwner(owner, minion){  
    globaInteligens[owner].addMinion(minion);
}