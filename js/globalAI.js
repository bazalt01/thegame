/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Класс интелектуального агента по управлению и координации ботов addMinion(minion)
 * @param {object} minion - Миньон
 */
function globalAI(nameAI, classAI){
    
    this.name = nameAI;
    this.classAI = classAI;
    gameMap.addAbsolutlyElement(this); 
    
    var minions = []; // Хранилище миньонов
    var aim = [];
    
    /**
     * Функция добавления миньонов в общий массив addMinion(minion). Данный массив хранит объекты, где объект состоит из: ссылки, ID, принятого пути, принятой главной цели агента.
     * @param {object} minion - Миньон
     */
    this.addMinion = function(minion){
        minions.push({minion: minion, id: minion.idObject, way:minion.way, porpose:minion.mainPorpose});
    };
    
    /**
     * Функция проверки траекторий всех принадлежащих ей агентов verificationTraektories()
     */
    this.verificationTraektories =  function(){
        // Проверка соответсвия параметров агента текущих к принятым (текущие параметры - это параметры установленные непосредственно у агента, принятые(одобренные) параметры - это параметры принятые владельцем агента)
        for(var i in minions){
            
            var agent = minions[i].minion;
            
            // Если у агента изменился путь, то идет его проверка на отсутствие мозможных пересечений с траекториями других объектов
            if(agent.way != minions[i].way){
                
                // Собираем потенциальную траекторию
                var x, y, w, h, k; // Вспомогательные переменные
                var traectorie = new Spots(agent.name,"wayC",""); // Объект центральная траектория            
                x = 0; y = 0; w = 0; h = 0;
                if(agent.way.X-agent.center.X > 0){x = agent.center.X; w = agent.way.X-agent.center.X;} 
                else if(agent.way.X-agent.center.X < 0){x = agent.way.X; w = agent.center.X-agent.way.X;}
                else{x = agent.way.X; w = 0;}
                    
                if(agent.way.Y-agent.center.Y > 0){y = agent.center.Y; h = agent.way.Y-agent.center.Y;} 
                else if(agent.way.Y-agent.center.Y < 0){y = agent.way.Y; h = agent.center.Y-agent.way.Y;}
                else{y = agent.way.Y; h = 0;}
            
                traectorie.addCoordinates(x,y);
                traectorie.addSize(w,h);
                
                // Проверка по созданой траектории
                var controlElements = [];  // Хранит проверенные объекты
                var sectors = gameMap.acrossSectors(traectorie); // Получаем сектора через которые проходит траектория
                var spotsArray = [];       // Хранит точки попадания в границы объетов
                
                for(var j in sectors){
                                                          
                    var sector = gameMap.getSector(sectors[j].i, sectors[j].j); // Получение каждого отдельного сектора
                    
                    for(var k in sector.elements){
                        
                        var element = sector.elements[k];
                        if(element.name != agent.name && element.deleted != true)   // Проверка, не является ли элемент собой же или удаленным с пространства
                        
                        if(!searchForMatches(controlElements, element)){  // Проверка на наличие повторного элемента

                            controlElements.push(element);  // Если в массиве controlElements нет совпадающего элемента то он добавляется

                            if(element.classObject == "element" || element.classObject == agent.classObject){  // Проверка соответсвия элемента определенным классам
                            
                                if(element.acrossObject(traectorie)){               // Наличие пересечения траектории с объектом
                                    
                                    var newSpot = new Spots("newSpot", "newSpot","");   // Объект пересечения траектории с границей объекта
                                    newSpot.addSize(1, 1);  

                                    edLineAcross(agent.way,agent.center,{X:element.coordinates.X, Y:0}, spotsArray, element, newSpot, traectorie);                            // Проверка пересечения с левой стороной
                                    edLineAcross(agent.way,agent.center,{X:element.coordinates.X+element.size.W, Y:0}, spotsArray, element, newSpot, traectorie);  // Проверка пересечения с правой стороной
                                    edLineAcross(agent.way,agent.center,{X:0, Y:element.coordinates.Y}, spotsArray, element, newSpot, traectorie);                            // Проверка пересечения с нижней стороной
                                    edLineAcross(agent.way,agent.center,{X:0, Y:element.coordinates.Y+element.size.H}, spotsArray, element, newSpot, traectorie);  // Проверка пересечения с верхней стороной                               
                                    delete newSpot;   
                                    
                                }
                            }
                        }    
                    }
                }
                delete traectorie; // Очистка памяти
                if(spotsArray.length > 0){agent.findWay();} // если нашли пересечение с будующей траекторией то агент заного ищет путь
                else{minions[i].way = agent.way;} // Иначе просто присваеваем траекторию как одобренную
            }
            
            // Если у агента изменилась цель
            if(agent.mainPorpose != minions[i].mainPorpose){
                
            } 
        }
    };
    
    /**
     * Функция добавления и обновления сведений о цели setAim(aimVisible)
     */
    this.setAim = function(aimVisible){
        if(aim.length == 0){aim.push(aimVisible);} // Проверка на налицие целей в базе агента
        else{
            var consilience = false; // Совпадение, вспомогательная переменная для фиксации совпадений по базе
            // Перебор базы целей
            for(var i in aim){
                // При совпадении целей просто обновляются координаты
                if(aim[i].aim == aimVisible.aim){
                    aim[i].coordinates = aimVisible.coordinates;    // Обновление координат
                    consilience = true;                             // Наличие совпадения
                }               
            }
            if(!consilience){aim.push(aimVisible);} // При отсутствии совпадений просто добавляем новую цель         
        }        
    };
    
    /**
     * Функция аналитики действий подчиненных агентов
     */
    this.analysesAction = function(){
                        
        for(var i in aim){
            if(!aim[i].aim.life()){
                var waitMinion = [];
                for(var j in minions){
                    if(!minions[j].minion.life()){
                        var minion = minions[j].minion;
                        for(var k in minion.fixedAim){
                            if(minion.fixedAim[k] == aim[i].aim){ // Если цель есть в списке обнаруженных агентом

                                if(minion.health > aim[i].aim.health && minion.fixedAim.length == 1){ // Если агент наблюдает всего одну цель, и у нее здоровья меньше, то он атакует ее
                                    minion.activationStatus("attack");
                                }
                                if(minion.fixedAim.length > 1 || minion.health <= aim[i].aim.health){ // Если наблюдаемых целей больше одной, то он переходит в режим ожидания и добавляется в массив агентов ждущих подмоги
                                    minion.activationStatus("wait");
                                    if(!searchForMatches(waitMinion, minion)){waitMinion.push(minion);}                           
                                }
                            }                   
                        }
                    }
                }
                var vAim = [];// Увиденные цели
                var freeMinions = []; // Свобоные агенты не имеющие фиксированных целей

                for(var j in waitMinion){
                    // Собираем всех увиденных текущими агентами целей
                    for(var k in waitMinion[j].fixedAim){
                        if(!searchForMatches(vAim, waitMinion[j].fixedAim[k])){vAim.push(waitMinion[j].fixedAim[k]);}   // Добавление целей в массив 
                    }
                }
                // Теперь надо узнать отношение количества увиденных целей агентами к их количеству
                if(waitMinion.length > vAim.length){ // По проверки всех агентов, если данную цель наблюдают более одного агента, то им дается статус атаковать
                    for(var j in waitMinion){
                        waitMinion[j].activationStatus("attack");
                    }
                }
                // Если видимых целей больше то ищем ближайший свободный агент без фиксированных им целей
                else if(vAim.length > 0){
                    for(var j in minions){
                        if(minions[j].minion.fixedAim.length == 0 && minions[j].minion.mainPorpose == undefined && !minions[j].minion.life()){freeMinions.push(minions[j].minion);} 
                    }
                    if(freeMinions.length > 0){
                        // Поиск наиближайшего агента
                        var minShortWay = Math.pow(10,10);
                        var nearest; // 
                        for(var j in freeMinions){
                            var shortWay = freeMinions[j].findShortWay(aim[i].aim.center);
                            if(minShortWay > shortWay){minShortWay = shortWay; nearest = freeMinions[j];}
                        }
                        nearest.mainPorpose = aim[i].aim.center;
                        nearest.activationStatus("step");
                    }
                    else{
                        for(var j in waitMinion){
                            waitMinion[j].activationStatus("attack");
                        }
                    }
                }
            }
        }
    };
    
    this.TimeTiсk = function()
    {          
        this.analysesAction();
        this.verificationTraektories();        
    };
}
