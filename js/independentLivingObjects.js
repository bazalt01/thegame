
/**
* Класс самостоятельных живых объектов independentLivingObjects(name, id_object, class_object)
* @param {string} name          - имя объекта
* @param {object} coordinates   - координаты объекта
* @param {string} id_object     - ID объекта
* @param {string} class_object  - класс объекта
* @param {object} size          - размеры объекта
* @param --------------
* @param {function} move(os_coord,move_to) - Функция изменения координат объектов, os_coord ось координат по которой будет смещение, move_to величина смещения
*/
function independentLivingObjects(name, id_object, class_object){
    independentInanimateObjects.superclass.constructor.call(this, name, id_object, class_object);
    this.move = function(os_coord, move_to){
        this.coordinates[os_coord]+=move_to;
    };
}