/*
 * Файл для описания общего апи взаимодействия
 */
var unitApi = function()
{
    this.publicID = "q" + Math.random();

    /**
     * Инициализирует работу с сетью
     * @returns {undefined}
     */
    function init()
    {
        CometServer().subscription("web_status", function(e, n){
            console.log(["event", e, n])
        })

        var thisObj = this
        this.last_event = []
        CometServer().subscription("web_remoteUnit_SatusArray", function(event, name)
        {
            for(var i in event.data)
            {
                comet_server_signal().emit("remoteUnit." + event.data[i].name, event.data[i].status);
            }
        })
    }

    init();



    /**
     * Отправка данных о состоянии объектоа
     * @param string id
     * @param object status
     * @returns void
     */
    this.sendSatus = function(id, status)
    {
        CometServer().web_pipe_send("web_remoteUnit", id, status)
    }

    /**
     * Отправка сразу масива данных о состоянии разных объектов
     * @param array data
     * @returns void
     */
    this.sendSatusArray = function(data)
    {
        CometServer().web_pipe_send("web_remoteUnit_SatusArray", data)
    }

    this.bindObjectArray = [];

    /**
     * Привязка локального объекта к удалёному
     * @param string name
     * @param object obj
     * @returns void
     */
    this.bind = function(name, obj){
        this.bindObjectArray.push({obj:obj, name:name});
    };

    this.TimeTiсk = function( time )
    {
        if(time % 2 === 0)
        {
            var sendData = []
            for(var i in this.bindObjectArray)
            {
                var val = this.bindObjectArray[i]
                sendData.push({name:val.name, status:  val.obj.getStatus()})
            }

            this.sendSatusArray(sendData);
        }
    }
}

