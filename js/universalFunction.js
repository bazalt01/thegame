/**
 * Функция описывает частный случай пересечения прямых вида: ax+by+c=0 и x+c=0(y+c=0)
 * Это не функция это "капец" лучше не спрашивать, она просто:
 * 1 находит координаты пересечени
 * 2 присваивает их тестируемуму объекту
 * 3 Забивает в массив точек пересечения траектории с границами
 * @param {object} object1          - первая точка траектории
 * @param {object} object2          - вторая точка траектории
 * @param {object} object3          - граница объекта
 * @param {array}  arrayAcrossSpot  - массив куда идет заполнение
 * @param {object} testElem         - объект пространства
 * @param {object} newSpot          - объект точка
 */
function edLineAcross(object2,object1,object3, arrayAcrossSpot, testElem, newSpot, traectory){
    // Находим точки пересечения
    var a = object2.Y-object1.Y;
    var b = object1.X-object2.X;
    var c = object1.Y*(object2.X-object1.X)-object1.X*(object2.Y-object1.Y);
    if(object3.Y == 0){object3.Y = -(a*object3.X+c)/b;}
    if(object3.X == 0){object3.X = -(b*object3.Y+c)/a;}
    var envirous = gameMap.getSizeEnvirons(); // Получаем границы пространства
    // Проверяем полученые координаты на наличие их в пределалах пространства
    if(object3.X < 0) object3.X = 1;
    if(object3.X > envirous.W) object3.X = envirous.W-1;
    if(object3.Y < 0) object3.Y = 1;
    if(object3.Y > envirous.H) object3.Y = envirous.H-1;
    //console.log(x, y);
    newSpot.addCoordinates(object3.X, object3.Y);   
    
    // Если точка в границах траектории то проверяем дальше
    if(traectory.acrossObject(newSpot)){
        // Если точка в границах объекта то добавляем в массив
        if(testElem.acrossObject(newSpot)){arrayAcrossSpot.push({X:object3.X, Y:object3.Y, link:testElem});}
    }
}

/**
 * Функция описывает частный случай пересечения прямых вида: ax+by+c=0 и x+c=0(y+c=0)
 * Это не функция это "капец" лучше не спрашивать, она просто:
 * 1 находит координаты пересечени
 * 2 присваивает их тестируемуму объекту
 * 3 Забивает в массив точек пересечения траектории с границами
 * @param {object} object1          - первая точка траектории
 * @param {object} object2          - вторая точка траектории
 * @param {object} object3          - граница объекта
 * @param {array}  arrayAcrossSpot  - массив куда идет заполнение
 * @param {object} testElem         - объект пространства
 * @param {object} newSpot          - объект точка
 */
function lineAcross(coordLine1, coordLine2){
    // Получаем уравнения прямых
    var line1 = equationOfLine(coordLine1);
    var line2 = equationOfLine(coordLine2);
    var x;
    var y;
    var testArrayForX = []; // Массив для хранения промежуточных решений
    var testArrayForY = []; // Массив для хранения промежуточных решений
    
    // Находим X
    testArrayForX[0] = line2.a;
    testArrayForX[1] = -(line2.b*line1.a/line1.b);
    testArrayForX[2] = (line2.b*line1.c/line1.b);
    testArrayForX[3] = -line2.c;
    console.log("0:",testArrayForX[0], "1:",testArrayForX[1], "2:",testArrayForX[2], "3:",testArrayForX[3]);
    for(var i in testArrayForX){
        if(typeof(testArrayForX[i]) != 'number' || (testArrayForX[i] > Math.pow(10,10) || testArrayForX[i] < -Math.pow(10,10))){ testArrayForX[i] = 0;}
    }
    x = (testArrayForX[2]+testArrayForX[3])/(testArrayForX[0]+testArrayForX[1]);
    
    // Находим Y
    testArrayForY[0] = line2.b;
    testArrayForY[1] = -(line2.a*line1.b/line1.a);
    testArrayForY[2] = (line2.a*line1.c/line1.a);
    testArrayForY[3] = -line2.c;
    console.log("0:",testArrayForY[0], "1:",testArrayForY[1], "2:",testArrayForY[2], "3:",testArrayForY[3]);
    for(var i in testArrayForY){
        if(typeof(testArrayForY[i]) != 'number' || (testArrayForY[i] > Math.pow(10,10) || testArrayForY[i] < -Math.pow(10,10))){ testArrayForY[i] = 0;}
    }
    
    y = (testArrayForY[2]+testArrayForY[3])/(testArrayForY[0]+testArrayForY[1]);
    
    console.log(x,y);
}
lineAcross({X1:-3, X2:6, Y1:1, Y2:7}, {X1:1, X2:7, Y1:8, Y2:-1});

/**
 * Пересечение двух отрезков 
 * http://otvety.google.ru/otvety/thread?tid=53e3d700aff620a5
 * http://acmp.ru/article.asp?id_text=170
 * @param {type} segment1
 * @param {type} segment2
 * @returns {x:x, y:y} Вернёт точку пересечения или undefined
 */
function segmentAcross(segment1, segment2){

    x1 = segment1.x1
    x2 = segment1.x2
    x3 = segment2.x1
    x4 = segment2.x2
    
    y1 = segment1.y1
    y2 = segment1.y2
    y3 = segment2.y1
    y4 = segment2.y2
    
    v1 =(x4-x3)*(y1-y3)-(y4-y3)*(x1-x3);
    v2 =(x4-x3)*(y2-y3)-(y4-y3)*(x2-x3);
    v3 =(x2-x1)*(y3-y1)-(y2-y1)*(x3-x1);
    v4 =(x2-x1)*(y4-y1)-(y2-y1)*(x4-x1);
    if( (v1*v2<0) && (v3*v4<0)  )
    {
        x=((x1*y2-x2*y1)*(x4-x3)-(x3*y4-x4*y3)*(x2-x1))/((y1-y2)*(x4-x3)-(y3-y4)*(x2-x1));
        y=((y3-y4)*x-(x3*y4-x4*y3))/(x4-x3);
        
        if( isNaN(y))
        {
              y = y1 // Работать будет только на ровных
        }

        x = -Math.floor(x*10000)/10000
        y = Math.floor(y*10000)/10000
        
        //testPoint({x:x, y:y}, "7f7")
        return {X:x, Y:y};
    }
    return  false; 
}

/**
 * Проверяет находится ли значение в заданом отрезке
 * @param {type} value
 * @param {type} x1
 * @param {type} x2
 * @returns {Boolean}
 */
function isIn(value, x1, x2){
    if(x1 < x2)
    {
        return value <= x2 && value >= x1
    }
    else
    {
        return value <= x1 && value >= x2
    }
}

function VectorAngle(v1, v2)
{
   vv1 = normVector(v1)
   vv2 = normVector(v2)
   return Math.acos( dotVector(vv1, vv2));
}

function normVector( v ){
    
    var len = lenVector(v);
    
    return {
        X:v.X/ len, Y:v.Y/ len
    }
}


function toRad( Angle ){
    return Angle * ((2*Math.PI)/360)// Перевод градусов в радианы
}

function toDeg( Angle ){
    //return Math.floor(Angle * 180/Math.PI);
    return Angle * 180/Math.PI;
}



/**
 * Скалярное произведение
 * @param {type} v1
 * @param {type} v2
 * @returns {undefined}
 */
function dotVector(v1, v2){
    return v1.X*v2.X + v1.Y*v2.Y
}

/**
 * Длина вектора
 * @param {type} v1
 * @param {type} v2
 * @returns {undefined}
 */
function lenVector(v){
    
    if(!v)
    { 
        throw new Exeption("v is undefined") 
    }
    
    r = Math.pow(v.X*v.X+v.Y*v.Y, 0.5)
    if(isNaN(r))
    {
        throw new Exeption("get nan") 
    }
    return r;
}

/**
 * Рисует точку в координатах x и y цвета color
 */
function testPoint(point, color){
    
    if(!point)
    {
        console.error("point is undefined")
        return;
    }
    
    if(point.x || point.y)
    {
        throw new Exeption("min x or min y")
    }
    
    if(!color)
    {
        color = "f77"
    }
    
    spot = new Spots("spot", "spot","");                // Объект дырка от пули на поверхности
    spot.addCoordinates(point.X, point.Y);
    spot.addSize(2, 2);
    spot.createElement("main");
    spot.addStyle();
    spot.addStyle("border", "3px solid #"+color);
     
}

function testSegment(segment){
    testPoint({X:segment.x1, Y:segment.y1})
    testPoint({X:segment.x2, Y:segment.y2})
}


/**
 * Функция возвращает коэффициенты от уравнения прямой
 * @param {object} coordLine - Координаты линии
 */
function equationOfLine(coordLine){
    var a = coordLine.Y2-coordLine.Y1;
    var b = coordLine.X1-coordLine.X2;
    var c = (coordLine.Y1*coordLine.X2)-(coordLine.X1*coordLine.Y2);
    console.log("a:",a, "b:",b, "c:",c);
    return {a:a, b:b, c:c};
}

/**
 * Функция возвращает случайное число от min до max
 * @param {number} min - минимум
 * @param {number} max - максимум
 */
function getRandomInt(min, max){
    return Math.floor(Math.random() * (max - min + 1)) + min;
}
/**
 * Функция перебирает все элементы сектора и возвращает true если на ходит пересечение
 * @param {array}   elements        - массив элементов
 * @param {object}  element_across  - проверяемый элемент
 * @param {object}  exception       - исключающий объект
 */
 function pereborElementAcross(elements, element_across, exception){

    for(var i in elements)
    {
        if(exception !== undefined){ if(elements[i].name == exception.name) continue;} // Проверка при наличии исключения
        if(elements[i].acrossObject(element_across) && elements[i].deleted != true)
        {
            //console.log(elements[i].name +" - "+element_across.name+" - "+this.name)
            
            // Если функция onAcross определена то её вызовем. Если она вернула true то пересечение не учитывается.
            if(elements[i].onAcross !== undefined && elements[i].onAcross(exception) !== true)
            {
                return elements[i];
            }
        }
    }
    
    return false;
}

/**
 * Функция поиска совпадений в массиве
 * @param {array}   array       - массив элементов
 * @param {object}  element     - проверяемый элемент
 * @param {object}  exception   - исключающий объект
 */
function searchForMatches(array, element, exception){
       
    for(var i in array){
        if(typeof(exception) != "undefined"){ if(array[i].name == exception.name) continue;} // Проверка при наличии исключения
        if(array[i] == element) return true;
    }
    
}

/**
 * Функция проверяющая положение объекта исключительно в пределах пространства, при выходе возвращает true
 * @param {object}  element     - проверяемый элемент
 */
function boundsCheckingEnvirons(element){
    var sizeEnvirons = gameMap.getSizeEnvirons();
    if(element.coordinates.X < 0 || element.coordinates.X+element.size.W > sizeEnvirons.W || element.coordinates.Y < 0 || element.coordinates.Y+element.size.H > sizeEnvirons.H){return true;}
}


/**
 * Функция проверяющая положение объекта исключительно в пределах пространства, при выходе возвращает true
 * @param {object}  beginCoord      - Координаты начала
 * @param {object}  endCoord        - Координаты конца
 * @param {number}  angle           - Угол разворота
 */
function angleRotate(beginCoord, endCoord, angle){
    var rx = endCoord.X - beginCoord.X;
    var ry = endCoord.Y - beginCoord.Y;
            
    var angleRad = angle*Math.PI/180; // Угол разворота в радианах
    var s1 = Math.cos(angleRad);
    var s2 = Math.sin(angleRad);
    var positionXRotate = Math.round(beginCoord.X + rx * s1 - ry * s2);   // Координата поворота по Х
    var positionYRotate = Math.round(beginCoord.Y + rx * s2 + ry * s1);   // Координата поворота по Y
    return {X: positionXRotate, Y: positionYRotate};
}

function getGipotenuza(katet1, katet2){
    return Math.pow((Math.pow(katet1,2)+Math.pow(katet2,2)),0.5);
}

/**
 * Функция возвращает пошаговые координа смещения объекта
 * @param {object}  begin      - Координаты начала
 * @param {object}  end        - Координаты конца
 * @param {number}  speed      - Скорость движения
 */
function processingStep(begin, end, speed){
     
    var w = Math.abs(begin.X - end.X);
    var h = Math.abs(begin.Y - end.Y);
    var k;
    if(w == 0 || h == 0){k = 1;}
    else{k = w/h;}
    
    var g = getGipotenuza(w,h);
    var l = g/w;
    if(w == 0){l = g/h;}
    if(h == 0){l = g/w;}
    var dx = 0; // направление движения по X
    var dy = 0; // направление движения по Y
                                                                            
    if(begin.X < end.X){dx = 1;}        // Направление движение вправо
    if(begin.X > end.X){dx = -1;}       // Направление движение влево
    if(begin.Y < end.Y){dy = 1;}        // Направление движение вниз
    if(begin.Y > end.Y){dy = -1;}       // Направление движение вверх
    

    var stepX = 0; // Шаг по X траектории
    var stepY = 0; // Шаг по Y траектории
    if((g) < speed){ // Если это последний шаг
        return {X:w*dx, Y:h*dy};
    }
    else{       
        stepX = speed/l;
        stepY = stepX/k;  
        if(w == 0){stepX = 0; stepY = speed;}
        if(h == 0){stepX = speed; stepY = 0;}
        return {X:stepX*dx, Y:stepY*dy};
    }
    
}
