/*
 * Класс управлямых удалённо объектов
 */


function remoteUnit(name, id_object, class_object)
{
    remoteUnit.superclass.constructor.call(this, name, id_object, class_object);
    extend(this, Wight, "remoteUnit -> Wight");
    
    /**
     * Последняя пришедшая команда
     */
    this.comands = []
    var thisObj = this
    comet_server_signal().connect("remoteUnit." + id_object, function(param, signal_name){

        if(thisObj.comands.length > 1)
        {
            var px = thisObj.comands[thisObj.comands.length - 1].pos.X;
            var py = thisObj.comands[thisObj.comands.length - 1].pos.Y;

            var sx = thisObj.comands[thisObj.comands.length - 1].speed.X;
            var sy = thisObj.comands[thisObj.comands.length - 1].speed.Y;

            px+=sx;
            py+=sy;

            sx = param.pos.X - px;
            sy = param.pos.Y - py;

            var i_status = {pos:{X:px, Y:py}, speed:{X:sx, Y:sy}}
            thisObj.comands.push(i_status);
        }

        thisObj.comands.push(param);

    }) 

    this.TimeTiсk = function( time )
    {
        if(this.comands.length > 2)
        {
            var lastComand = this.comands[this.comands.length - 2]
            if(lastComand.pos)
            {
                this.setPos(lastComand.pos)
                lastComand.pos = undefined;

            }

            if(lastComand.speed)
            {
                this.status.speed = lastComand.speed
                this.step(this.status.speed)
            }

            if(lastComand.num_use )
            {
                lastComand.speed = undefined;
            }
            else if(lastComand.num_use === undefined)
            {
                lastComand.num_use = 1
            }
        }

        if(this.comands.length > 10)
        {
            this.comands = this.comands.slice(4, this.comands.length);
        }
    }

}
    extend(remoteUnit, Wight, "remoteUnit -> Wight");