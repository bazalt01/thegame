
/**
* Класс самостоятельных неживые объектов - independentInanimateObjects(name, id_object, class_object). Является подклассом ObjectOfEnvirons
* @param {string} name           - имя объекта
* @param {object} coordinates    - координаты объекта
* @param {string} id_object      - ID объекта
* @param {string} class_object   - класс объекта
* @param {object} size           - размеры объекта
* @param {DOM} element           - DOM элемент
* @param -----------------
* @param {function} move(oc_coord,speed,width_way) - Функция смещения объекта по определенной оси
*/
function independentInanimateObjects(name, id_object, class_object){
    independentInanimateObjects.superclass.constructor.call(this, name, id_object, class_object);
    extend(this, ObjectOfEnvirons); 

    /**
     * Функция смещения объекта по определенной оси
     * @param {string} oc_coord  - ось координат
     * @param {number} speed     - скорость смещения
     * @param {number} width_way - растояние смещения
     */
    this.move = function(oc_coord, speed, width_way){

        var elem = this; // Сам объект
        function moveElement(elem, oc_coord, speed, width_way){

            if(oc_coord === "Y"){
                elem.coordinates.Y+=speed;
                elem.element.style.top = (elem.coordinates.Y)+"px";
                if((elem.coordinates.Y) === width_way){
                    clearInterval(timer);
                }
            }
            if(oc_coord === "X"){
                elem.coordinates.X+=speed;
                elem.element.style.left = (elem.coordinates.X)+"px";
                if((elem.coordinates.X) === width_way){
                    clearInterval(timer);
                }
            }
            // ------------- Отладочная информация --------------------------------------------------------------------------------------------------------------//
            elem.visibleCoord(); // Отображение текущих координат объекта
            // --------------------------------------------------------------------------------------------------------------------------------------------------//
        }
        // Осуществление задержки перед шагом смещения
        var timer = setInterval(function(){moveElement(elem, oc_coord, speed, width_way);}, 50);
    };
    
    this.TimeTiсk = function( )
    {
        return;
        //stepObject(this);
    }
}

extend(independentInanimateObjects, ObjectOfEnvirons); 