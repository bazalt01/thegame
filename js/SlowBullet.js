/*
 * Класс медленных пуль
 */


SlowBullet = function(parentObject)
{
    var class_object = "SlowBullet";
    var id_object = "Bullet_"+Math.random();
    var name = id_object;

    SlowBullet.superclass.constructor.call(this, name, id_object, class_object);
    extend(this, Bonus);

    this.parentObject = parentObject;

    this.health = 1
    this.status.speed_factor = 4;



    
    
    var pos = this.parentObject.GetCoordinates();
    
    var len = this.parentObject.getSize().h;
    if(len < this.parentObject.getSize().w)
    {
        len = this.parentObject.getSize().w;
    }
    
    len+= 1;
    
    var sx = Math.cos(this.parentObject.status.rotate);
    var sy = Math.sin(this.parentObject.status.rotate);
    
    var x = pos.X + (sx*len) + this.parentObject.getSize().w/2
    var y = pos.Y + (sy*len) + this.parentObject.getSize().h/2
    
    this.status.speed = {X:sx * this.status.speed_factor, Y:sy * this.status.speed_factor}
    
    this.createElement();
    this.addCoordinates(x , y);
    this.setRotate(this.parentObject.getRotate() );
    this.addSize(14, 7);
    this.addStyle();
    this.addStyle("backgroundImage", "url(img/bullet2.png)");
    this.addStyle("border", "1px solid #ccc");
    
      //  console.log("SlowBulletFire", x , y, "[sx:"+sx+", sy:"+sy+"]")
}

    SlowBullet.prototype.TimeTiсk = function( time )
    {
        var lastAcross = this.step(this.status.speed.X, this.status.speed.Y);
        if(lastAcross !== false)
        {
            this.acrossTo(lastAcross);
        }
    }

    SlowBullet.prototype.onAcross = function(element){
        return this.acrossTo(element);
    }

    SlowBullet.prototype.acrossTo = function(element){
        
        if(element && element.name && this.parentObject && this.parentObject.name){
            if(element.name === this.parentObject.name)
            {
                console.log("not acrossTo:"+element.name)
                return true; // Не учитывать пересечение с родительским объектом
            }

            console.log("onAcross", element.name);
            element.health -= 70;

            gameMap.removeElement(this);
            return true;
        }
    }
     

    /**
     * Выводит координаты объекта внутри него самого (Для отладки)
     */
    SlowBullet.prototype.visibleCoord = function(){    };


    extend(SlowBullet, Bonus);