/**
 * Spots описывает любые объекты созданные на поверхности не хранящиеся в массиве пространства 
 * @param {string} name
 * @param {string} id_object
 * @param {string} class_object
 */
Spots = function(name, id_object, class_object){
    Spots.superclass.constructor.call(this, name, id_object, class_object);
    extend(Spots, ObjectOfEnvirons); 
    
    /**
     * Создание и добавления div элемента на пространство - createElement(parent)
     * @param {string} parent - id пространства
     */
    this.createElement = function(parent){
        this.element = document.createElement('div');               // Создание нового элемента div
        this.element.id = this.idObject;                            // Присвоение ID
        document.getElementById(parent).appendChild(this.element);  // Добавление элемента к родителю
    };
};


extend(Spots, ObjectOfEnvirons);                        // Наследование Spots от ObjectOfEnvirons
